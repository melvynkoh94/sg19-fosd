package com.fosd.app.utilities;

public class Constants {
	public final static long INITIAL_DELAY = 0; 
	public final static long PERIOD_INTERVAL = 5000;  
	public final static int BB_INTERVAL = 10;
	
	public final static String generateTradeOrderMessage(boolean isBuy, long id, double currentPrice, int size, String stockName, String timestamp) {
		return "{\"buy\":" + isBuy + ", \"id\":" + id + ", \"price\":" + currentPrice + 
				", \"size\":" + size + ", \"stock\":\"" + stockName + "\", \"whenAsDate\":\"" + timestamp + "\"}";
	}
}
