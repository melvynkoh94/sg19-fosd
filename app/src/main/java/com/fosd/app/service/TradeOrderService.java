package com.fosd.app.service;

import java.util.Collection;

import com.fosd.app.entity.TradeOrder;


public interface TradeOrderService {
	
	//Records a stock into the TradeOrderRepository.
	void recordTradeOrder(TradeOrder tradeOrder);
		
	public Iterable<TradeOrder> retrieveAllTradeOrders();
	
	public void insertAllTradeOrders(Collection<TradeOrder> collection);

	TradeOrder retrieveTradeOrder(long id);
	
	public void updateTradeOrderStatus(long id, String isActive);
	
}
