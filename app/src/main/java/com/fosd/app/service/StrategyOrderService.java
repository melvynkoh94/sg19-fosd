package com.fosd.app.service;

import java.util.List;

import com.fosd.app.entity.Strategy;
import com.fosd.app.entity.StrategyOrder;

public interface StrategyOrderService {
	
	public void recordStrategyOrder(StrategyOrder stratOrder);
	public List<StrategyOrder> retrieveStrategyOrders();
	StrategyOrder retrieveStrategyOrder(long id);
	Strategy retrieveStrategy(long id, String name);
	public void updateStrategyOrder(long id, String isActive);
}
