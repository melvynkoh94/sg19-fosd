package com.fosd.app.service;

import java.util.Collection;

import com.fosd.app.entity.PriceBreakoutStrategy;

public interface PBStrategyService {
	//Records a stock into the PBRepository
	public void recordPBStrategy(PriceBreakoutStrategy PBStrategy);
	
	//Retrieves BB Strategy order based on the ID
	public PriceBreakoutStrategy retrievePBStrategy(long Id);

	public Iterable<PriceBreakoutStrategy> retrieveAllPBStrategies();
	
	public void insertAllPBStrategies(Collection<PriceBreakoutStrategy> collection);
}
