package com.fosd.app.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fosd.app.entity.Stock;
import com.fosd.app.entity.StockPrice;
import com.fosd.app.repository.StockPriceRepository;
import com.fosd.app.repository.StockRepository;
import com.fosd.app.utilities.Constants;

@Service
public class FeedServiceImpl implements FeedService {
	private static final Logger LOGGER = LoggerFactory.getLogger(FeedServiceImpl.class);	


	@Autowired
	private StockRepository stockRepository; 
	@Autowired
	private StockPriceRepository stockPriceRepository;

	@Autowired
	private StrategyManager strategyManager;
	
	private String stockListUrl = "http://feed2.conygre.com/API/StockFeed/GetSymbolList";
	private String url = "http://feed2.conygre.com/API/StockFeed/GetStockPricesForSymbol/%s?HowManyValues=1";

	private RestTemplate restTemplate = new RestTemplate();

	public FeedServiceImpl() {}

	@PostConstruct
	public void UpdateStockList () {
		LOGGER.debug("Setting up stock list");
		ArrayList<LinkedHashMap<Integer, Entry<String,String>>> response = restTemplate.getForObject(stockListUrl, ArrayList.class);
		
		JSONArray json = new JSONArray(response);
		
		for (int i = 0; i < json.length(); i ++) {
			String symbol = json.getJSONObject(i).getString("Symbol").toLowerCase();
			if(stockRepository.findBySymbol(symbol) == null) {
				String name = json.getJSONObject(i).getString("CompanyName").toLowerCase();
				stockRepository.save(new Stock(name.toLowerCase(), symbol));
			}
		}
	}

	@Override
	public void start() {
		ScheduledExecutorService es = Executors.newScheduledThreadPool(1);
		es.scheduleAtFixedRate(this::scheduleFetch, Constants.INITIAL_DELAY, Constants.PERIOD_INTERVAL, TimeUnit.MILLISECONDS);
	}

	private void scheduleFetch() {
		//		Iterable<Stock> stockList = stockRepository.findAll();
		Stock [] stockList = new Stock [] {
				new Stock("Apple" , "aapl"),
				new Stock("Google" , "goog"),
				new Stock("BerkShire Hathaway" , "brk_b"),
				new Stock("Norfolk Southern Corp" , "nsc"),
				new Stock("Microsoft" , "msft")
		};

		for (Stock s : stockList) {
			//			Runnable runnable = () -> {
			String symbol = s.getSymbol().toLowerCase();
			String result = restTemplate.getForObject(String.format(url, symbol), String.class);
			JSONObject obj = new JSONArray(result).getJSONObject(0);
			try {
				double price = obj.getDouble("Price");
				StockPrice stockPrice = new StockPrice(price, LocalDateTime.now());
				stockPrice.setStock(stockRepository.findBySymbol(symbol));
				stockPriceRepository.save(stockPrice);
			} catch (Exception e) {
				LOGGER.error("Error Occured While Fetching Prices : {}", e.getCause());
			}
			//			};
			//			Thread thread = new Thread(runnable);
			//			thread.start();
		}
		
//		Runnable runnable = () -> {
		strategyManager.manageTradeOrders();
//		};
//		Thread thread = new Thread (runnable);
//		thread.start();
	}
}
