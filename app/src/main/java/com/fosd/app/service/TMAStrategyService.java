package com.fosd.app.service;

import java.util.Collection;
import com.fosd.app.entity.TwoMovingAverageStrategy;

public interface TMAStrategyService {
	
	//Records a stock into the TMAStrategyRepository
	public void recordTMAStrategy(TwoMovingAverageStrategy TMAStrategy);
	
	//Retrieves trade order based on the ID
	public TwoMovingAverageStrategy retrieveTMAStrategy(long Id);

	public Iterable<TwoMovingAverageStrategy> retrieveAllTMAStrategies();
	
	public void insertAllTMAStrategies(Collection<TwoMovingAverageStrategy> collection);
}
