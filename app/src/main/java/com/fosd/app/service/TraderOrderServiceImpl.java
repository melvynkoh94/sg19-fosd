package com.fosd.app.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fosd.app.entity.TradeOrder;
import com.fosd.app.repository.TradeOrderRepository;

@Service
public class TraderOrderServiceImpl implements TradeOrderService {

	@Autowired
	private TradeOrderRepository repository;

	@Override
	public void recordTradeOrder(TradeOrder tradeOrder) {
		// TODO Auto-generated method stub
		repository.save(tradeOrder);
	}

	@Override
	public TradeOrder retrieveTradeOrder(long id) {
		// TODO Auto-generated method stub
		return repository.findById(id).get();
	}
	
	@Override
	public Iterable<TradeOrder> retrieveAllTradeOrders() {
		// TODO Auto-generated method stub
		return repository.findAll();
	}

	@Override
	public void insertAllTradeOrders(Collection<TradeOrder> collection) {
		repository.saveAll(collection);
	}

	@Override
	public void updateTradeOrderStatus(long id, String status) {
		TradeOrder order = repository.findById(id).get();
		order.setStatus(status);
		repository.save(order);
	}	
}
