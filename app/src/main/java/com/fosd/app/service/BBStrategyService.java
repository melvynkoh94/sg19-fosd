package com.fosd.app.service;

import java.util.Collection;

import com.fosd.app.entity.BoillingersBandStrategy;


public interface BBStrategyService {
	//Records a stock into the BBRepository
	public void recordBBStrategy(BoillingersBandStrategy BBStrategy);
	
	//Retrieves BB Strategy order based on the ID
	public BoillingersBandStrategy retrieveBBStrategy(long Id);

	public Iterable<BoillingersBandStrategy> retrieveAllBBStrategies();
	
	public void insertAllBBStrategies(Collection<BoillingersBandStrategy> collection);

}
