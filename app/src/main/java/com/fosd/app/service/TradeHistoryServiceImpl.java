package com.fosd.app.service;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fosd.app.entity.TradeHistory;
import com.fosd.app.repository.TradeHistoryRepository;

@Service
public class TradeHistoryServiceImpl implements TradeHistoryService {
	
	@Autowired
	private TradeHistoryRepository repository; 

	@Override
	public void recordTradeOrder(TradeHistory tradeHistory) {
		repository.save(tradeHistory);
	}

	@Override
	public TradeHistory retrieveTradeOrder(String name) {
		return null;
	}

	@Override
	public List<TradeHistory> retrieveTradeHistoryByStrategyOrderId(long id) {
		return repository.findByStrategyOrderId(id);
	}
	
	@Override
	public Iterable<TradeHistory> retrieveAllTradeHistory() {
		return repository.findAll();
	}

	@Override
	public void insertAllTradeHistory(Collection<TradeHistory> collection) {
		repository.saveAll(collection);
	}
}
