package com.fosd.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fosd.app.entity.Stock;
import com.fosd.app.entity.StockPrice;
import com.fosd.app.repository.StockPriceRepository;

@Service
public class StockPriceServiceImpl implements StockPriceService {

	@Autowired
	StockPriceRepository repository;
	
	@Override
	public StockPrice findLatestPriceByStock(Stock stock) {
		List<StockPrice> stockPriceList = repository.findByStockOrderByIdDesc(stock);
		return stockPriceList.size() != 0 ? stockPriceList.get(0) : null;
	}

	@Override
	public List<StockPrice> findAllPriceByStock(Stock stock) {
		return repository.findByStockOrderByIdDesc(stock);
	}

}
