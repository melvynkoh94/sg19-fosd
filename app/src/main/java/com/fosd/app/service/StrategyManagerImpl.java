package com.fosd.app.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.json.JSONObject;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fosd.app.entity.StockPrice;
import com.fosd.app.entity.Strategy;
import com.fosd.app.entity.TradeHistory;
import com.fosd.app.entity.TradeOrder;
import com.fosd.app.mockOrderBroker.OrderBrokerSender;
import com.fosd.app.repository.TradeHistoryRepository;
import com.fosd.app.repository.TradeOrderRepository;
import com.fosd.app.utilities.Constants;
import com.fosd.app.utilities.Tuple;

import ch.qos.logback.classic.Logger;

@Service
public class StrategyManagerImpl implements StrategyManager{
	
	private static final Logger LOGGER = (Logger) LoggerFactory.getLogger(StrategyManagerImpl.class);

	@Autowired
	private TradeOrderRepository tradeOrderRepository;
	@Autowired
	private OrderBrokerSender orderBrokerSender;
	@Autowired
	TradeHistoryRepository tradeHistoryRepository;

	@Override
	public void manageTradeOrders() {
		ArrayList<List<Strategy>> strategiesList = new ArrayList<List<Strategy>>();
		strategiesList.add(tradeOrderRepository.findTradeOrdersWithStrategyOrderWithTwoMovingAverageStrategyJoined()); 
		strategiesList.add(tradeOrderRepository.findTradeOrdersWithStrategyOrderWithBoillingersBandStrategyJoined()); 
		strategiesList.add(tradeOrderRepository.findTradeOrdersWithStrategyOrderWithPriceBreakoutStrategyJoined()); 

		for (List<Strategy> list : strategiesList) {
			List<Strategy> stream = list.stream().filter(order -> order.getStrategyOrder().getTradeOrder().getStatus().equals("active") && !order.getStrategyOrder().getTradeOrder().isProcessing()).collect(Collectors.toList());
			for (Strategy strategy: stream) {
				Tuple<Boolean,String> next = strategy.doNextStep();
				if	 (next._1) {
					String nextPosition = next._2;

					TradeOrder tradeOrder = strategy.getStrategyOrder().getTradeOrder();
					List<StockPrice> stockPriceList = strategy.getStrategyOrder().getStock().getStockPrices();
					boolean isEntering = strategy.getStrategyOrder().getTradeOrder().getIsEntering();

					boolean isBuy;
					long id = tradeOrder.getId();
					double currentPrice = stockPriceList.get(stockPriceList.size() - 1).getPrice();
					int size = tradeOrder.getSizeToTrade();
					String stockName = tradeOrder.getStrategyOrder().getStock().getSymbol();
					String timestamp = LocalDateTime.now().toString();

					if (tradeOrder.getPosition().equals("closed")) {
						if ((tradeOrder).getBasePrice() == 0) {
							tradeOrder.setBasePrice(currentPrice);
						}
						isBuy = nextPosition.equals("short") ? false : true;
					} else {
						if (isEntering && tradeOrder.getPosition().equals("short") 
								|| !isEntering && tradeOrder.getPosition().equals("long")) {
							isBuy = false;
						} else {
							isBuy = true;
						}
					}

					tradeOrder.setPosition(nextPosition);
					tradeOrder.setProcessing(true);
					tradeOrderRepository.save(tradeOrder);

					String message = Constants.generateTradeOrderMessage(isBuy, id, currentPrice, size, stockName, timestamp);
					String correlationID = UUID.randomUUID().toString();
					orderBrokerSender.sendOrderBrokerMessage(message, correlationID);
				}
			}	
		}
	}

	@Override
	public void receiveBrokerMessage(String message)  {
		JSONObject replyJSON = new JSONObject(message);     

		boolean isBuy = replyJSON.getBoolean("buy");
		long id = replyJSON.getLong("id");
		double priceTraded = replyJSON.getDouble("price");
		int sizeTraded = replyJSON.getJSONObject("result").getInt("SharesExecuted");
		LocalDateTime timestamp = LocalDateTime.parse(replyJSON.getString("whenAsDate"));
		String result = replyJSON.getJSONObject("result").getString("Request");
		
		TradeHistory tradeHistory = new TradeHistory();
		tradeHistory.setIsLongPosition(isBuy);
		tradeHistory.setSizeTraded(sizeTraded);
		tradeHistory.setPrice(priceTraded);
		tradeHistory.setResult(result);
		tradeHistory.setTimestamp(timestamp);

		TradeOrder tradeOrder = tradeOrderRepository.findById(id).orElseGet(null);
		if (tradeOrder == null) {
			LOGGER.error("Trade Order should not be null!");
		} else {
			tradeHistory.setStrategyOrder(tradeOrder.getStrategyOrder());
			
			//Also need to set tradeOrder details --> isPartialFilled, isProcessing, tradeSize, basePrice, isEntering
			tradeOrder.setProcessing(false);
			
			if (sizeTraded +  tradeOrder.getSizeTraded() < tradeOrder.getStrategyOrder().getSizeOrdered()) {
				tradeOrder.setIsPartialFilled(true);
				tradeOrder.setSizeToTrade(tradeOrder.getStrategyOrder().getSizeOrdered() - tradeOrder.getSizeTraded() - sizeTraded);
				tradeOrder.setSizeTraded(tradeOrder.getSizeTraded() + sizeTraded);
				
			} else {
				tradeOrder.setIsPartialFilled(false);
				tradeOrder.setSizeToTrade(tradeOrder.getStrategyOrder().getSizeOrdered());
				tradeOrder.setSizeTraded(0);
				if (tradeOrder.getIsEntering() == false) {
					tradeOrder.setIsEntering(true);
					tradeOrder.setPosition("closed");
					if (tradeOrder.getBasePrice() == 0) {
						tradeOrder.setBasePrice(priceTraded);
					}
				} else {
					tradeOrder.setIsEntering(false);
				}
			}
			
			tradeOrderRepository.save(tradeOrder);
			tradeHistoryRepository.save(tradeHistory);
		}
	}
}
