package com.fosd.app.service;

public interface StrategyManager {

	void manageTradeOrders();
	void receiveBrokerMessage(String message);
}
