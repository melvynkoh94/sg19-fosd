package com.fosd.app.service;

import java.util.Collection;
import java.util.List;

import com.fosd.app.entity.Stock;

public interface StockService {
	
	//returns stock price based on the stated stock symbol
	double calculateCurrentStockPrice(String Symbol);
	
	//Records a stock into the respective repository.
	void recordStock(Stock stock);
	
	//Retrieves stock from the respective repository
	Stock retrieveStock(String stockSymbol);
	
	public List<Stock> retrieveAllStocks();

	void insertAllStocks(Collection<Stock> collection);

}
