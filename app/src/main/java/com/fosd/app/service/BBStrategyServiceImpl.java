package com.fosd.app.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fosd.app.entity.BoillingersBandStrategy;
import com.fosd.app.repository.BBStrategyRepository;

@Service
public class BBStrategyServiceImpl implements BBStrategyService {
	
	@Autowired
	BBStrategyRepository repository;

	@Override
	public void recordBBStrategy(BoillingersBandStrategy BBStrategy) {
		repository.save(BBStrategy);
	}

	@Override
	public BoillingersBandStrategy retrieveBBStrategy(long Id) {
		return null;
	}

	@Override
	public Iterable<BoillingersBandStrategy> retrieveAllBBStrategies() {
		return repository.findAll();
	}

	@Override
	public void insertAllBBStrategies(Collection<BoillingersBandStrategy> collection) {
		repository.saveAll(collection);
	}

}
