package com.fosd.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fosd.app.entity.Strategy;
import com.fosd.app.entity.StrategyOrder;
import com.fosd.app.repository.BBStrategyRepository;
import com.fosd.app.repository.PBStrategyRepository;
import com.fosd.app.repository.StrategyOrderRepository;
import com.fosd.app.repository.TMAStrategyRepository;

@Service
public class StrategyOrderServiceImpl implements StrategyOrderService {
	
	@Autowired
	private StrategyOrderRepository strategyRepository;
    @Autowired
    private TMAStrategyRepository tmaRepository;
    @Autowired
    private BBStrategyRepository bbRepository;
    @Autowired
    private PBStrategyRepository pbRepository;
	
    @Override
    public Strategy retrieveStrategy(long id, String name) {
        if (name == "TMA") {
            return tmaRepository.findById(id).get();
        } else if (name == "BB") {
            return bbRepository.findById(id).get();
        } else if (name == "PB") {
            return pbRepository.findById(id).get();
        } else {
            return null;
        }
    }
    
	@Override
	public void recordStrategyOrder(StrategyOrder stratOrder) {
		strategyRepository.save(stratOrder);
	}

	@Override
	public StrategyOrder retrieveStrategyOrder(long id) {
		return strategyRepository.findById(id).orElseGet(null);
	}
	
	@Override
	public List<StrategyOrder> retrieveStrategyOrders() {
		return (List<StrategyOrder>) strategyRepository.findAll();
	}

	@Override
	public void updateStrategyOrder(long id, String status) {
		StrategyOrder order = strategyRepository.findById(id).get();
		order.setStatus(status);
		strategyRepository.save(order);	
	}	
}

	
