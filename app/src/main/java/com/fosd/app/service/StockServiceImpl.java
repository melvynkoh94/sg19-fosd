package com.fosd.app.service;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fosd.app.entity.Stock;
import com.fosd.app.repository.StockRepository;

@Service
public class StockServiceImpl implements StockService {

	@Autowired
	StockRepository repository;

	@Override
	public double calculateCurrentStockPrice(String Symbol) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void recordStock(Stock stock) {
		// TODO Auto-generated method stub
		repository.save(stock);
	}

	@Override
	public Stock retrieveStock(String stockSymbol) {
		return repository.findBySymbol(stockSymbol);
	}

	@Override
	public List<Stock> retrieveAllStocks() {
		return (List<Stock>) repository.findAll();
	}


	@Override
	public void insertAllStocks(Collection<Stock> collection) {
		repository.saveAll(collection);
	}

}