package com.fosd.app.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fosd.app.entity.TwoMovingAverageStrategy;
import com.fosd.app.repository.TMAStrategyRepository;

@Service
public class TMAStrategyServiceImpl implements TMAStrategyService {
	
	@Autowired
	private TMAStrategyRepository repository;

	@Override
	public void recordTMAStrategy(TwoMovingAverageStrategy TMAStrategy) {
		repository.save(TMAStrategy);
	}

	@Override
	public TwoMovingAverageStrategy retrieveTMAStrategy(long Id) {
		return null;
	}

	@Override
	public Iterable<TwoMovingAverageStrategy> retrieveAllTMAStrategies() {
		return repository.findAll();
	}

	@Override
	public void insertAllTMAStrategies(Collection<TwoMovingAverageStrategy> collection) {
		repository.saveAll(collection);
	}
}
