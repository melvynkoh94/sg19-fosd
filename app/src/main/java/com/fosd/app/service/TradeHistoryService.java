package com.fosd.app.service;

import java.util.Collection;
import java.util.List;

import com.fosd.app.entity.TradeHistory;;

public interface TradeHistoryService {
	//Records a stock into the TradeOrderRepository.
	public void recordTradeOrder(TradeHistory tradeHistory);
	
	//Retrieves trade order based on the ID
	public TradeHistory retrieveTradeOrder(String name);

	public Iterable<TradeHistory> retrieveAllTradeHistory();
	
	public void insertAllTradeHistory(Collection<TradeHistory> collection);

	List<TradeHistory> retrieveTradeHistoryByStrategyOrderId(long id);
}
