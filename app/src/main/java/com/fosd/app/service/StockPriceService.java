package com.fosd.app.service;

import java.util.List;

import com.fosd.app.entity.Stock;
import com.fosd.app.entity.StockPrice;


public interface StockPriceService {

	StockPrice findLatestPriceByStock(Stock stock);
	
	List<StockPrice> findAllPriceByStock(Stock stock);
}
