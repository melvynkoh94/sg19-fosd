package com.fosd.app.service;

import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fosd.app.entity.PriceBreakoutStrategy;
import com.fosd.app.repository.PBStrategyRepository;

@Service
public class PBStrategyServiceImpl implements PBStrategyService{
	@Autowired
	PBStrategyRepository repository;

	@Override
	public void recordPBStrategy(PriceBreakoutStrategy PBStrategy) {
		repository.save(PBStrategy);
	}

	@Override
	public PriceBreakoutStrategy retrievePBStrategy(long Id) {
		return null;
	}

	@Override
	public Iterable<PriceBreakoutStrategy> retrieveAllPBStrategies() {
		return repository.findAll();
	}

	@Override
	public void insertAllPBStrategies(Collection<PriceBreakoutStrategy> collection) {
		repository.saveAll(collection);
	}
}
