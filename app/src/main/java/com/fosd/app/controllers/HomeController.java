package com.fosd.app.controllers;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fosd.app.entity.BoillingersBandStrategy;
import com.fosd.app.entity.PriceBreakoutStrategy;
import com.fosd.app.entity.StrategyOrder;
import com.fosd.app.entity.TradeHistory;
import com.fosd.app.entity.TradeOrder;
import com.fosd.app.entity.TwoMovingAverageStrategy;
import com.fosd.app.repository.BBStrategyRepository;
import com.fosd.app.repository.PBStrategyRepository;
import com.fosd.app.repository.TMAStrategyRepository;
import com.fosd.app.service.BBStrategyService;
import com.fosd.app.service.PBStrategyService;
import com.fosd.app.service.StockService;
import com.fosd.app.service.StrategyOrderService;
import com.fosd.app.service.TMAStrategyService;
import com.fosd.app.service.TradeHistoryService;
import com.fosd.app.service.TradeOrderService;


/**
 * Controller for the different actions from the user
 * @author User
 *
 */
@RequestMapping(value = "/")
@RestController
@CrossOrigin
public class HomeController {
	private static final Logger LOGGER = LoggerFactory.getLogger(HomeController.class);	

	@Autowired
	private StrategyOrderService strategyOrderService;
	@Autowired
	private TradeOrderService tradeOrderService;
	@Autowired
	private TradeHistoryService tradeHistoryService;
	@Autowired
	private TMAStrategyService tmaStrategyService;
	@Autowired
	private PBStrategyService pbStrategyService;
	@Autowired
	private BBStrategyService bbStrategyService;
	@Autowired
	private StockService stockService;
	@Autowired
	private TMAStrategyRepository TMARepo;
	@Autowired
	private BBStrategyRepository BBRepo;
	@Autowired
	private PBStrategyRepository PBRepo;

	private void validateStrategyOrder(StrategyOrder order) {
		checkNotNull(order.getStock());
		checkNotNull(order.getName());
	}

	@RequestMapping(value = "/TMAconfirm",  method = RequestMethod.POST)
	public ResponseEntity<String> postTMAOrder(@RequestBody StrategyOrder order, String symbol, int shortWindow, int longWindow, int interval){
		LOGGER.debug("POST /TMAconfirm api called");
		LOGGER.info("Inside postTMAOrder");
		order.setStock(stockService.retrieveStock(symbol.toLowerCase()));
		try {
			validateStrategyOrder(order);
		} catch(Exception e) {
			LOGGER.error("Mandatory field (Stock, Name) for Strategy Order is null");
		}

		String position = "closed";
		int size = order.getSizeOrdered();
		String status = order.getStatus();
		boolean isEntering = true;

		TwoMovingAverageStrategy TMAStrategy = new TwoMovingAverageStrategy(shortWindow, longWindow, interval);

		TradeOrder tradeOrder = new TradeOrder(position, size, status, isEntering, 0, false);
		order.setTradeOrder(tradeOrder);
		TMAStrategy.setStrategyOrder(order);
		tradeOrder.setStrategyOrder(order);
		tmaStrategyService.recordTMAStrategy(TMAStrategy);

		return ResponseEntity.ok("Two Moving Average Strategy Order added!");
	}

	@RequestMapping(value = "/BBconfirm",  method = RequestMethod.POST)
	public ResponseEntity<String> postBBOrder(@RequestBody StrategyOrder order, String symbol, int stdDeviation, int window){
		LOGGER.debug("POST /BBconfirm api called");

		order.setStock(stockService.retrieveStock(symbol.toLowerCase()));
		try {
			validateStrategyOrder(order);
		} catch(Exception e) {
			LOGGER.debug("Mandatory field (Symbol, Price) for stock is null");
			return ResponseEntity.badRequest().body("Mandatory field (Sybmol, Price) for stock is null");
		}
		strategyOrderService.recordStrategyOrder(order);

		String position = "closed";
		int size = order.getSizeOrdered();
		String isActive = order.getStatus();
		boolean isEntering = true;
		
		BoillingersBandStrategy BBStrategy = new BoillingersBandStrategy(stdDeviation, window);
		TradeOrder tradeOrder = new TradeOrder(position, size, isActive, isEntering, 0, false);

		order.setTradeOrder(tradeOrder);
		BBStrategy.setStrategyOrder(order);
		tradeOrder.setStrategyOrder(order);
		bbStrategyService.recordBBStrategy(BBStrategy);

		return ResponseEntity.ok("BB Strategy Order added!");		
	}
	
	@RequestMapping(value = "/PBconfirm",  method = RequestMethod.POST)
	public ResponseEntity<String> postPBOrder(@RequestBody StrategyOrder order, String symbol, int period){
		LOGGER.debug("POST /PBconfirm api called");
		order.setStock(stockService.retrieveStock(symbol.toLowerCase()));
		try {
			validateStrategyOrder(order);
		} catch(Exception e) {
			LOGGER.debug("Mandatory field (Stock, Name) for stock is null");
			return ResponseEntity.badRequest().body("Mandatory field (Stock, name) for stock is null");
		}
		//strategyOrderService.recordStrategyOrder(order);

		String position = "closed";
		int size = order.getSizeOrdered();
		String isActive = order.getStatus();
		boolean isEntering = true;
		
		PriceBreakoutStrategy PBStrategy = new PriceBreakoutStrategy(period);
		TradeOrder tradeOrder = new TradeOrder(position, size, isActive, isEntering, 0, false);

		order.setTradeOrder(tradeOrder);
		PBStrategy.setStrategyOrder(order);
		tradeOrder.setStrategyOrder(order);
		pbStrategyService.recordPBStrategy(PBStrategy);

		return ResponseEntity.ok("BB Strategy Order added!");		
	}
	
	@RequestMapping(value = "/tradeOrders/{strategyName}/test/{id}", method = RequestMethod.GET, produces = "application/json", headers="Accept=application/json, application/xml")
	public Map<String, Object> getStrategyDetails(@PathVariable("strategyName") String strategyName, @PathVariable("id") int id) {
		Map<String, Object> map = new HashMap<String, Object>();
		//LOGGER.info("Inside getStrategyDetails");
		if(strategyName.equals("TMA")) {
			//TwoMovingAverageStrategy tmaOrder = strategyOrderService.retrieveStrategyOrder(id);
			//TwoMovingAverageStrategy tmaOrder = tmaStrategyService.retrieveTMAStrategy(id);
			//TwoMovingAverageStrategy tmaOrder = TMARepo.findByStrategyOrder(stratOrder);
			TwoMovingAverageStrategy tmaOrder = TMARepo.findByStrategyOrderId(id);
			map.put("short_window", tmaOrder.getShortAvgWindow());
			map.put("long_window", tmaOrder.getLongAvgWindow());
			map.put("period", tmaOrder.getPeriod());
			map.put("size_to_trade", tmaOrder.getStrategyOrder().getTradeOrder().getSizeToTrade());
			return map;			
		}
		else if(strategyName.equals("BB")) {
			//BoillingersBandStrategy bbOrder = bbStrategyService.retrieveBBStrategy(id);
			BoillingersBandStrategy bbOrder = BBRepo.findByStrategyOrderId(id);	
			map.put("std_deviation", bbOrder.getStdDeviation());
			map.put("window", bbOrder.getAvgWindow());
			map.put("size_to_trade", bbOrder.getStrategyOrder().getTradeOrder().getSizeToTrade());
			return map;
		}
		else if(strategyName.equals("PB")) {
			//PriceBreakoutStrategy pbOrder = pbStrategyService.retrievePBStrategy(id);
			PriceBreakoutStrategy pbOrder = PBRepo.findByStrategyOrderId(id);
			map.put("period", pbOrder.getPeriod());
			map.put("size_to_trade", pbOrder.getStrategyOrder().getTradeOrder().getSizeToTrade());
			return map;
		}	
		return map;
	}
	
	@RequestMapping(value = "/tradeOrders", method = RequestMethod.GET, produces = "application/json")
	public Map<String, Object> getTradeOrders(){
		LOGGER.debug("GET /tradeOrders api called");
		List<TradeOrder> tradeOrderList = (List<TradeOrder>) tradeOrderService.retrieveAllTradeOrders();
		//Map<TradeOrder, Stock> map = tradeOrderList.stream().collect(HashMap::new (m,x) -> m.put(x, stockService.findLatestPriceByStock(x)), HashMap::putAll)
		
		Map<String, Object> result = new HashMap<String, Object>();
		ArrayList<Object> list = new ArrayList<Object>();
		Map<Long, Double> po = profitsPerStrategy();
		
		for (TradeOrder tOrder : tradeOrderList) {
			
			//TRADE ORDER
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("trade_id", tOrder.getId());
			map.put("base_price", tOrder.getBasePrice());
			map.put("trade_isEntering", tOrder.getIsEntering());
			map.put("trade_isPartialFilled", tOrder.getIsPartialFilled());
			map.put("trade_position", tOrder.getPosition());
			map.put("trade_sizeToTrade", tOrder.getSizeToTrade());
			//STRATEGY ORDER
			map.put("strategy_name", tOrder.getStrategyOrder().getName());
			map.put("strategy_order_id", tOrder.getStrategyOrder().getId());
			map.put("stategy_order_status", tOrder.getStrategyOrder().getStatus());
			map.put("time_ordered", tOrder.getStrategyOrder().getTimestamp());
			//STOCKS
			map.put("stock_symbol", tOrder.getStrategyOrder().getStock().getSymbol());
			map.put("stock_name", tOrder.getStrategyOrder().getStock().getName());
			//PERF
			map.put("performance", po.get(tOrder.getStrategyOrder().getId()));
			list.add(map);	
		}
		result.put("response",  list);
		return result;
	}
	
	@RequestMapping(value = "/totalProfits", method = RequestMethod.GET, produces = "application/json")
    public Map<String, Double> getTotalProfits(){
        LOGGER.debug("GET /totalProfits api called");
        List<TradeHistory> tradeHistoryList = (List<TradeHistory>) tradeHistoryService.retrieveAllTradeHistory();

        double totalProfits = tradeHistoryList.stream().filter(x->x.getResult()!="Rejected").mapToDouble(x->{
            return x.getIsLongPosition() ? -1 * x.getSizeTraded() * x.getPrice() : x.getSizeTraded() * x.getPrice();
        }).sum();
        
        Map<String, Double> results = new HashMap<String, Double>();
        results.put("response", totalProfits);
        return results;
	}
	
    private Map<Long, Double> profitsPerStrategy(){
        LOGGER.debug("GET /profitsPerStrategy api called");
        List<StrategyOrder> strategyOrder = strategyOrderService.retrieveStrategyOrders();
        Map<Long, Double> map = new HashMap<Long, Double>();
        for (StrategyOrder so : strategyOrder) {
        	List<TradeHistory> thList = tradeHistoryService.retrieveTradeHistoryByStrategyOrderId(so.getId());
        	double sum = 0;
        	long id = so.getId();
        	for (TradeHistory th: thList) {
        		if (th.getResult() != "Rejected") {
        			if (th.getIsLongPosition()) {
        				sum += -1 * th.getSizeTraded() * th.getPrice();
        			} else {
        				sum +=  th.getSizeTraded() * th.getPrice();
        			}
        		}
        	}
        	map.put(id, sum);
        }
        
        return map;
	}
	
	@RequestMapping(value = "/allTradeHistory", method = RequestMethod.GET, produces = "application/json")
	public Map<String, Object> getTradeHistory(){
		LOGGER.debug("GET /allTradeHistory api called");
		List<TradeHistory> tradeHistoryList = (List<TradeHistory>) tradeHistoryService.retrieveAllTradeHistory();
		
		Map<String, Object> result = new HashMap<String, Object>();
		ArrayList<Object> list = new ArrayList<Object>();
		for (TradeHistory hist : tradeHistoryList) {
			Map<String, Object> map = new HashMap<String, Object>();
			//TRADE HISTORY
			map.put("trade_id",hist.getId());
	        map.put("trade_isBuy",hist.getIsLongPosition());
	        map.put("trade_sizeTraded",hist.getSizeTraded());
	        map.put("trade_price", hist.getPrice());
	        map.put("trade_timestamp", hist.getTimestamp());
	        map.put("trade_result",hist.getResult());
	        //STRATEGY ORDER
	        map.put("stategy_order_id",hist.getStrategyOrder().getId());
	        map.put("stategy_name",hist.getStrategyOrder().getName());
	        //STOCKS
	        map.put("stock_name",hist.getStrategyOrder().getStock().getName());
	        map.put("stock_symbol",hist.getStrategyOrder().getStock().getSymbol());
	      
	        list.add(map);
		}
        
        result.put("response",list);

		return result;
	}

	//Update status of Trade Order and Strategy Order
	@RequestMapping(value = "/updateTradeOrder/{id}", method = RequestMethod.PUT, headers="Accept=application/json, application/xml")
	public void postTradeOrder(@PathVariable long id, String status){
		
		LOGGER.debug("PUT /updateTradeOrder/%s api called", id);
		tradeOrderService.updateTradeOrderStatus(id, status);
		strategyOrderService.updateStrategyOrder(tradeOrderService.retrieveTradeOrder(id).getStrategyOrder().getId(), status);
		tradeOrderService.retrieveTradeOrder(id).getStrategyOrder().setStatus(status);
	}	
	
	
	@RequestMapping(value = "/updateTMA/{id}", method = RequestMethod.PUT, headers="Accept=application/json, application/xml")
	public @ResponseBody void updateTMAStrategy(@PathVariable long id, int shortAvgWindow, int longAvgWindow, int interval) {
		TwoMovingAverageStrategy tmaOrder = TMARepo.findByStrategyOrderId(id);
		
		//TwoMovingAverageStrategy TMAStrategy = tmaStrategyService.retrieveTMAStrategy(id);
		tmaOrder.setShortAvgWindow(shortAvgWindow);
		tmaOrder.setLongAvgWindow(longAvgWindow);
		tmaOrder.setPeriod(interval);
    	tmaStrategyService.recordTMAStrategy(tmaOrder);	
	}
	
	
	@RequestMapping(value = "/updateBB/{id}", method = RequestMethod.PUT, headers="Accept=application/json, application/xml")
	public @ResponseBody void updateBBStrategy(@PathVariable long id, int stdDeviation, int window) {
		BoillingersBandStrategy bbOrder = BBRepo.findByStrategyOrderId(id);	
		
		//BoillingersBandStrategy BBStrategy = bbStrategyService.retrieveBBStrategy(id);
		bbOrder.setStdDeviation(stdDeviation);
		bbOrder.setAvgWindow(window);
		bbStrategyService.recordBBStrategy(bbOrder);
	}
	
	
	@RequestMapping(value = "/updatePB/{id}", method = RequestMethod.PUT, headers="Accept=application/json, application/xml")
	public @ResponseBody void updatePBStrategy(@PathVariable long id, int period) {
		PriceBreakoutStrategy pbOrder = PBRepo.findByStrategyOrderId(id);
		
		//PriceBreakoutStrategy PBStrategy = pbStrategyService.retrievePBStrategy(id);
		pbOrder.setPeriod(period);
		pbStrategyService.recordPBStrategy(pbOrder);
	}	
}
