package com.fosd.app.controllers;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fosd.app.entity.Stock;
import com.fosd.app.entity.StockPrice;
import com.fosd.app.service.StockPriceService;
import com.fosd.app.service.StockService;

@RequestMapping(value = "/")
@RestController
@CrossOrigin
public class StockController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(StockController.class);
	
	@Autowired
	StockService stockService;
	@Autowired
	StockPriceService stockPricesService;
	 
	@RequestMapping(value = "/chooseStocks", method = RequestMethod.POST)
	public ResponseEntity<String> postStock(@RequestBody Stock stock){
		LOGGER.debug("POST /chooseStocks api called");
		try {
			validateStock(stock);
		} catch (Exception e) {
			LOGGER.debug("Mandatory field (Symbol, Price) for stock is null");
			return ResponseEntity.badRequest().body("Mandatory field (Sybmol, Price) for stock is null");
		}
		stockService.recordStock(stock);
		return ResponseEntity.ok("Stock added");	
	}
	
	private void validateStock(Stock stock) {
        checkNotNull(stock.getSymbol());
    }
	
	@RequestMapping(value = "/displayAllStocks", method = RequestMethod.GET)
    public Map<String, Object> getAllStocks() {
        LOGGER.debug("GET /displayAllStocks api called");
        List<Stock> stockList = (List<Stock>) stockService.retrieveAllStocks();
        List<Object> allStocksList = new ArrayList<Object>();
        Map<String, Object> result = new HashMap<String, Object>();
        for (Stock stock : stockList) {
        	Map<String,Object> map = new HashMap<String,Object>();
        	map.put("stock_id",stock.getId());
	        map.put("stock_name",stock.getName());
	        map.put("stock_symbol",stock.getSymbol());
	        map.put("stock_price_latest", stockPricesService.findLatestPriceByStock(stock) != null? stockPricesService.findLatestPriceByStock(stock).getPrice() : 0);
	        
	        allStocksList.add(map);
        }
        result.put("response", allStocksList);
       
        return result;
    }
	
	@RequestMapping(value = "/stock", method = RequestMethod.GET)
    public Stock getStock(@RequestParam String symbol) {
        LOGGER.debug("GET /stock api called, symbol: {}", symbol);
        
        Stock stock = stockService.retrieveStock(symbol);
        return stock;
    }
}
