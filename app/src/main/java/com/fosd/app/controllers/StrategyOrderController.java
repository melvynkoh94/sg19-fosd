package com.fosd.app.controllers;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fosd.app.entity.Strategy;
import com.fosd.app.entity.StrategyOrder;
import com.fosd.app.service.StockService;
import com.fosd.app.service.StrategyOrderService;

@RequestMapping(value = "/home")
@RestController
@CrossOrigin
public class StrategyOrderController {

	@Autowired
	private StrategyOrderService strategyOrderService;

	private static final Logger LOGGER = LoggerFactory.getLogger(StrategyOrderController.class);

	//Store a StrategyOrder submitted by a user
	@RequestMapping(value = "/submitStrategyOrder",  method = RequestMethod.POST, headers= {"Content-Type=application/json, application/xml", "Accept=application/json, application/xml"})
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<String> addOrder(@RequestBody StrategyOrder order){
		//StrategyName, StockName, sizeOrdered, Timestamp,ShortWindow, LongWindow
		LOGGER.debug("POST /submitStrategyOrder api called");
		try {
			validateStrategyOrder(order);
		} catch(Exception e) {
			LOGGER.debug("Mandatory field (Symbol, Price) for stock is null");
			return ResponseEntity.badRequest().body("Mandatory field (Symbol, Price) for stock is null");
		}
		strategyOrderService.recordStrategyOrder(order);

		return ResponseEntity.ok("Strategy Order added");		
	}

	private void validateStrategyOrder(StrategyOrder order) {
		checkNotNull(order.getStock());
		checkNotNull(order.getSizeOrdered());
	}

	@RequestMapping(value = "/strategy", method = RequestMethod.GET)
    public Strategy getStrategyOrder(@RequestParam long id, String name){
        LOGGER.debug("GET /strategyOrder/{} api called", id);
        
        Strategy strategyOrder = strategyOrderService.retrieveStrategy(id, name);
        return strategyOrder;
    }
	
	@RequestMapping(value = "/strategyOrder", method = RequestMethod.GET)
	public StrategyOrder getStrategyOrder(@RequestParam long id){
		LOGGER.debug("GET /strategyOrder/%s api called", id);

		StrategyOrder strategyOrder = strategyOrderService.retrieveStrategyOrder(id);
		return strategyOrder;
	}

	@RequestMapping(value = "/strategyOrders", method = RequestMethod.GET)
	public Collection<StrategyOrder> getStrategyOrders(){
		LOGGER.debug("GET /strategyOrders api called");
		return strategyOrderService.retrieveStrategyOrders();
	}
	
}
