package com.fosd.app.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.fosd.app.entity.Stock;
import com.fosd.app.entity.StockPrice;

@Repository
public interface StockPriceRepository extends CrudRepository<StockPrice, Long> {
	List<StockPrice> findByStockOrderByIdDesc(Stock stock);
}