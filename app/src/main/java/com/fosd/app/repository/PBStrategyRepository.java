package com.fosd.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.fosd.app.entity.PriceBreakoutStrategy;
import com.fosd.app.entity.TwoMovingAverageStrategy;

@Repository
public interface PBStrategyRepository extends CrudRepository<PriceBreakoutStrategy, Long>{
	PriceBreakoutStrategy findByStrategyOrderId(long id);
}
