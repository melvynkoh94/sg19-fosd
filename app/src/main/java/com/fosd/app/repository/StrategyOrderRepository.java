package com.fosd.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.fosd.app.entity.StrategyOrder;
import com.fosd.app.entity.TradeOrder;

@Repository
public interface StrategyOrderRepository extends CrudRepository<StrategyOrder, Long> {
}
