package com.fosd.app.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.fosd.app.entity.Stock;

@Repository
public interface StockRepository extends CrudRepository<Stock, Long> {
	Stock findBySymbol(String symbol);
}
