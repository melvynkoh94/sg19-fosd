package com.fosd.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.fosd.app.entity.Stock;
import com.fosd.app.entity.StrategyOrder;
import com.fosd.app.entity.TwoMovingAverageStrategy;

@Repository
public interface TMAStrategyRepository extends CrudRepository<TwoMovingAverageStrategy, Long> {
	
	TwoMovingAverageStrategy findByStrategyOrderId(long id);
	TwoMovingAverageStrategy findByStrategyOrder(StrategyOrder order);
}
//
//public interface StockRepository extends CrudRepository<Stock, Long> {
//	Stock findBySymbol(String symbol);
//}
