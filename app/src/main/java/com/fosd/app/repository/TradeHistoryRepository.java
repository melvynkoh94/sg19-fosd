package com.fosd.app.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.fosd.app.entity.TradeHistory;

@Repository
public interface TradeHistoryRepository extends CrudRepository<TradeHistory, Long> {
	List<TradeHistory> findByStrategyOrderId(long id);
}

