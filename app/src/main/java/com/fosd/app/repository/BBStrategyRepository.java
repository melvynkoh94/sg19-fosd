package com.fosd.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.fosd.app.entity.BoillingersBandStrategy;
import com.fosd.app.entity.TwoMovingAverageStrategy;

@Repository
public interface BBStrategyRepository extends CrudRepository<BoillingersBandStrategy, Long> {
	BoillingersBandStrategy findByStrategyOrderId(long id);
}
