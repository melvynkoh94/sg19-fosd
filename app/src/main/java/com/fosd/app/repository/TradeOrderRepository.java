package com.fosd.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.fosd.app.entity.Strategy;
import com.fosd.app.entity.TradeOrder;

@Repository
public interface TradeOrderRepository extends CrudRepository<TradeOrder, Long> {	
	@Query("select r1 from TwoMovingAverageOrder r1 join r1.strategyOrder")
	List<Strategy> findTradeOrdersWithStrategyOrderWithTwoMovingAverageStrategyJoined();
	
	@Query("select r1 from BoillingerOrder r1 join r1.strategyOrder")
	List<Strategy> findTradeOrdersWithStrategyOrderWithBoillingersBandStrategyJoined();
	
	@Query("select r1 from PriceBreakoutOrder r1 join r1.strategyOrder")
	List<Strategy> findTradeOrdersWithStrategyOrderWithPriceBreakoutStrategyJoined();
}
