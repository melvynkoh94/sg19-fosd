package com.fosd.app.mockOrderBroker;

import java.text.ParseException;
import java.time.LocalDateTime;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.support.JmsHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import com.fosd.app.entity.TradeHistory;
import com.fosd.app.service.StrategyManager;


@Component
public class OrderBroker_ReplyReciever {
	
	@Autowired
	StrategyManager strategyManager;
	
	@JmsListener(destination="TradeOrder_ReplyDest")	
	public void receiveOrderBroker_ReplyMessage(@Header(JmsHeaders.CORRELATION_ID) String correlationId, 
												@Payload String message) throws ParseException {
		System.out.println("\n*********** Order Broker Response Recieved **************************************");
		System.out.println("Response Message received: " + message);
        System.out.println("Order Message Correlation ID: " + correlationId);
        
        
        strategyManager.receiveBrokerMessage(message);
	}
}
