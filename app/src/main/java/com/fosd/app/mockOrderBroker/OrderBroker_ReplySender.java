package com.fosd.app.mockOrderBroker;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component
public class OrderBroker_ReplySender {
	
	@Autowired
	private JmsTemplate jmsTemplate;
	
	public void sendOrderBroker_ReplyMessage(String message, String correlationID) { 
		
		if (message==null || correlationID == null) {
			throw new IllegalArgumentException("Invalid message: Message or Correlation ID not found. Please try again.");
		}
		
		System.out.println("\n*********** Order Broker Response Sent *****************************************");
		System.out.println("Response Details: " + message);
		System.out.println("Order Message Correlation ID: " + correlationID);

		jmsTemplate.convertAndSend("TradeOrder_ReplyDest", message, m->{
        	m.setJMSCorrelationID(correlationID);
        	return m;
        }); 
                
	}
}