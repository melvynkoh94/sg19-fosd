package com.fosd.app.mockOrderBroker;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.support.JmsHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class OrderBrokerReciever {

	@Autowired
	private OrderBroker orderBroker;
	
	@JmsListener(destination="TradeOrderDest")	
	public void receiveOrderBrokerMessage(@Header(JmsHeaders.CORRELATION_ID) String correlationId, 
												@Payload String message) {
        
		System.out.println("\n*********** Trade Order Recieved by Broker *************************************");
		System.out.println("Trade Order recieved by Broker: " + message);
        System.out.println("Order Message Correlation ID: " + correlationId);
        
        orderBroker.generateOrderBrokerResponse(message, correlationId);
        
	}
	 
}

