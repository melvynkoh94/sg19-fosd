package com.fosd.app.mockOrderBroker;

import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Random;

import org.json.JSONObject;

public class RandomTradeOrderGenerator {
	
	private Boolean buy;
	private int id;
	private double price; 
	private int size; 
	private String stock;
	private LocalDateTime whenAsDate;	
	
	private static String[] stockTickerNames = { "GOOG", "MSFT", "ACME", "OSL", "SCFC" };
	
	    
    public static JSONObject generateRandomTradeOrderJSON() {
    	Random r = new Random();
    	JSONObject obj = new JSONObject();
    	obj.put("buy",true);
        obj.put("id", 999);
        obj.put("price", r.nextDouble()*10);
        obj.put("size", 500);
        obj.put("stock", stockTickerNames[r.nextInt(stockTickerNames.length)]);
        obj.put("whenAsDate", LocalDateTime.now());
        return obj;
    }
    
    
	public static String generateRandomTradeOrder() {
		Random r = new Random();
		RandomTradeOrderGenerator m = new RandomTradeOrderGenerator();		
		m.buy = true;
		m.id = 999;
		m.price = r.nextDouble()*10;
		m.size = 500;
		m.stock = stockTickerNames[r.nextInt(stockTickerNames.length)];
		m.whenAsDate = LocalDateTime.now();
		
		return m.toString();
	}
	
	public static String generateFixedTradeOrder() {
		RandomTradeOrderGenerator m = new RandomTradeOrderGenerator();		
		m.buy = true;
		m.id = 999;
		m.price = 120.20;
		m.size = 500;
		m.stock = "MSFT";
		m.whenAsDate = LocalDateTime.now();
		
		return m.toString();
	}
	
	// Getters and setters, needed for Jackson message converters.
	public Boolean getBuy() {
		return buy;
	}
	public void setBuy(Boolean buy) {
		this.buy = buy;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public String getStock() {
		return stock;
	}
	public void setStock(String stock) {
		this.stock = stock;
	}
	public LocalDateTime getWhenAsDate() {
		return whenAsDate;
	}
	public void setWhenAsDate(LocalDateTime whenAsDate) {
		this.whenAsDate = whenAsDate;
	}
	
	@Override
	public String toString() {
		DecimalFormat df = new DecimalFormat("#0.00000");
		return "{\"buy\":" + buy + ", \"id\":" + id + ", \"price\":" + df.format(price) + 
				", \"size\":" + size + ", \"stock\":\"" + stock + "\", \"whenAsDate\":\"" + whenAsDate + "\"}";
	}
}
