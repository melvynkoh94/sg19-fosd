package com.fosd.app.mockOrderBroker;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component
public class OrderBrokerSender {
	
	@Autowired
	private JmsTemplate jmsTemplate;
	
	public void sendOrderBrokerMessage(String message, String correlationID) { 
		
		if (message==null || correlationID == null) {
			throw new IllegalArgumentException("Invalid message: Message or Correlation ID not found. Please try again.");
		}
		
		System.out.println("\n*********** Pending Trade Order Request ****************************************");
		System.out.println("Trade Order Details: " + message);
		System.out.println("Order Message Correlation ID: " + correlationID);

		jmsTemplate.convertAndSend("TradeOrderDest", message, m->{
        	m.setJMSCorrelationID(correlationID);
        	return m;
        }); 
                 
	}
	
}
