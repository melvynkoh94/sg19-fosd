package com.fosd.app.mockOrderBroker;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class OrderBroker {
	
	@Autowired
	private OrderBroker_ReplySender sender;
	
	public static double FILLED_SUCCESS_RATE = 0.9;
	public static double PARTIAL_FILLED_SUCCESS_RATE = 0.4;
	
	public void generateOrderBrokerResponse(String message, String correlationID) {
		
		JSONObject order = new JSONObject(message);
        order = brokerResponse(order, FILLED_SUCCESS_RATE, PARTIAL_FILLED_SUCCESS_RATE);     
        message = order.toString(); 
        sender.sendOrderBroker_ReplyMessage(message, correlationID);
        
	}
	
	public static JSONObject brokerResponse(JSONObject order, double filledRate, double patialFilledRate) throws IllegalArgumentException  {
		  
		  if (filledRate>1 || patialFilledRate>1 || filledRate<0 || patialFilledRate<0) { 
			  throw new IllegalArgumentException("Invalid Rate. Rates must be between 0 and 1");
		  }
		  
		  int size = order.getInt("size");
		  Map result = new LinkedHashMap(2);
		  Random r = new Random(); 
		  
		  if (r.nextDouble() <= filledRate) { 
			  result.put("Request", "Filled");
			  result.put("SharesExecuted", size);
		  } 
		  else if (r.nextDouble() <= patialFilledRate) { 
			  result.put("Request", "Partial Filled");
			  result.put("SharesExecuted", r.nextInt(size));
		 } 
		  else { 
			  result.put("Request", "Rejected");
			  result.put("SharesExecuted", 0);
		  }
		  order.put("result", result);
		  return order;
	  }
	
}

