package com.fosd.app.entity;

import com.fosd.app.utilities.Tuple;

public interface Strategy {
	Tuple<Boolean, String> doNextStep();
	StrategyOrder getStrategyOrder();
}
