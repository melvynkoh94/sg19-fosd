package com.fosd.app.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity(name = "Stock")
@Table(name="stock") 
public class Stock {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	private String name;
	private String symbol;

	@OneToMany(mappedBy="stock", cascade= CascadeType.ALL, orphanRemoval=true)
	private List<StrategyOrder> strategyOrders = new ArrayList<StrategyOrder>();

	@OneToMany(mappedBy="stock", cascade= CascadeType.ALL, orphanRemoval=true, fetch = FetchType.EAGER)
	private List<StockPrice> stockPrices = new ArrayList<StockPrice>();

	public Stock() {}

	public Stock(String name, String symbol) {
		this.name = name;
		this.symbol = symbol;
	}

	public void addStrategyOrder(StrategyOrder strategyOrder) {
		strategyOrders.add(strategyOrder);
		strategyOrder.setStock(this);
	}

	public void removeStrategyOrder(StrategyOrder strategyOrder) {
		strategyOrders.remove(strategyOrder);
		strategyOrder.setStock(null);
	}

	public void addStockPrice(StockPrice stockPrice) {
		stockPrices.add(stockPrice);
		stockPrice.setStock(this);
	}

	public void removeStockPrice(StockPrice stockPrice) {
		stockPrices.remove(stockPrice);
		stockPrice.setStock(null);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public List<StrategyOrder> getStrategyOrders() {
		return strategyOrders;
	}

	public void setStrategyOrders(List<StrategyOrder> strategyOrders) {
		this.strategyOrders = strategyOrders;
	}

	public List<StockPrice> getStockPrices() {
		return stockPrices;
	}

	public void setStockPrices(List<StockPrice> stockPrices) {
		this.stockPrices = stockPrices;
	}

	@Override
	public String toString() {
		return String.format("%d,%s,%s",id, name,symbol);
	}
}
