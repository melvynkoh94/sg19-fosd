package com.fosd.app.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity(name="TradeOrder")
@Table(name="trade_order") 
public class TradeOrder {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	private String position;
	private int tradeSize;
	private String status;
	private boolean isEntering;
	private double basePrice;
	private boolean isPartialFilled;
	
	@Transient
	private boolean isProcessing;

	@Transient
	private int sizeTraded = 0;
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "strategy_order_id")
	private StrategyOrder strategyOrder;
	
	public TradeOrder() {}

	public TradeOrder(String position, int sizeToTrade, String status, boolean isEntering,
			double basePrice, boolean isPartialFilled) {
		this.position = position;
		this.tradeSize = sizeToTrade;
		this.status = status;
		this.isEntering = isEntering;
		this.basePrice = basePrice;
		this.isPartialFilled = isPartialFilled;
		this.isProcessing = false;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public int getSizeToTrade() {
		return tradeSize;
	}

	public void setSizeToTrade(int sizeToTrade) {
		this.tradeSize = sizeToTrade;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public boolean getIsEntering() {
		return isEntering;
	}

	public void setIsEntering(boolean isEntering) {
		this.isEntering = isEntering;
	}

	public double getBasePrice() {
		return basePrice;
	}

	public void setBasePrice(double basePrice) {
		this.basePrice = basePrice;
	}

	public boolean getIsPartialFilled() {
		return isPartialFilled;
	}

	public void setIsPartialFilled(boolean isPartialFilled) {
		this.isPartialFilled = isPartialFilled;
	}

	public boolean isProcessing() {
		return isProcessing;
	}

	public void setProcessing(boolean isProcessing) {
		this.isProcessing = isProcessing;
	}

	public StrategyOrder getStrategyOrder() {
		return strategyOrder;
	}
	
	public void setStrategyOrder(StrategyOrder strategyOrder) {
		this.strategyOrder = strategyOrder;
	}

	public int getSizeTraded() {
		return sizeTraded;
	}

	public void setSizeTraded(int sizeTraded) {
		this.sizeTraded = sizeTraded;
	}
}