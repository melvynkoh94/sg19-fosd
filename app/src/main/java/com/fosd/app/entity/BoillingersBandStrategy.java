package com.fosd.app.entity;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fosd.app.utilities.Constants;
import com.fosd.app.utilities.Tuple;

@Entity(name="BoillingerOrder")
@Table(name="boillinger_order")
public class BoillingersBandStrategy implements Strategy{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private int stdDeviation;
	private int avgWindow;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="strategy_order_id")
	private StrategyOrder strategyOrder;

	public BoillingersBandStrategy() {}

	public BoillingersBandStrategy(int stdDeviation, int avgWindow) {
		this.stdDeviation = stdDeviation;
		this.avgWindow = avgWindow;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getStdDeviation() {
		return stdDeviation;
	}

	public void setStdDeviation(int stdDeviation) {
		this.stdDeviation = stdDeviation;
	}

	public int getAvgWindow() {
		return avgWindow;
	}

	public void setAvgWindow(int avgWindow) {
		this.avgWindow = avgWindow;
	}

	public void setStrategyOrder(StrategyOrder strategyOrder) {
		this.strategyOrder = strategyOrder;
	}

	@Override
	public StrategyOrder getStrategyOrder() {
		return strategyOrder;
	}

	@Override
	public Tuple<Boolean, String> doNextStep() {
		if (strategyOrder.getTradeOrder().getIsPartialFilled()) {
			return new Tuple<Boolean, String>(true,strategyOrder.getTradeOrder().getPosition());
		}

		List<Double> stockPriceList = strategyOrder.getStock().getStockPrices().stream().map(x -> x.getPrice()).collect(Collectors.toList());

		int buckets = avgWindow / Constants.BB_INTERVAL; 
		int numberOfSkips = (int) (Constants.BB_INTERVAL * 1000 / Constants.PERIOD_INTERVAL);
		double price = 0;
		double sum = 0;
		for (int i = 0; i <= buckets * numberOfSkips; i = i + numberOfSkips) {
			price += stockPriceList.get(stockPriceList.size() - 1 - i);
		}	
		double avg = price / buckets;
		for (int i = 0; i <= buckets * numberOfSkips; i = i + numberOfSkips) {
			sum += Math.pow((stockPriceList.get(stockPriceList.size() - 1 - i) - avg), 2);
		} 

		double standardDeviation = Math.sqrt((sum / buckets));
		double deviation = stdDeviation * standardDeviation;

		if (avg + deviation <= stockPriceList.get(stockPriceList.size() - 1) && !strategyOrder.getTradeOrder().getPosition().equals("short")) {
			return new Tuple<Boolean, String>(true, "short");
		} else if (avg - deviation >= stockPriceList.get(stockPriceList.size() - 1) && !strategyOrder.getTradeOrder().getPosition().equals("long")) {
			return new Tuple<Boolean, String>(true, "long");
		}

		return new Tuple<Boolean, String>(false, null);
	}
}
