package com.fosd.app.entity;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fosd.app.utilities.Constants;
import com.fosd.app.utilities.Tuple;

@Entity(name="PriceBreakoutOrder")
@Table(name="price_breakout_order")
public class PriceBreakoutStrategy implements Strategy{
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;	
	private int period;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="strategy_order_id")
	private StrategyOrder strategyOrder;

	public PriceBreakoutStrategy() {}
	
	public PriceBreakoutStrategy(int period) {
		this.period = period;
	}

	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public int getPeriod() {
		return period;
	}


	public void setPeriod(int period) {
		this.period = period;
	}


	public StrategyOrder getStrategyOrder() {
		return strategyOrder;
	}


	public void setStrategyOrder(StrategyOrder strategyOrder) {
		this.strategyOrder = strategyOrder;
	}

	@Override
	public Tuple<Boolean, String> doNextStep() {
		if (strategyOrder.getTradeOrder().getIsPartialFilled()) {
			return new Tuple<Boolean, String>(true,strategyOrder.getTradeOrder().getPosition());
		}
		
		List<Double> stockPriceList = strategyOrder.getStock().getStockPrices().stream().map(x -> x.getPrice()).collect(Collectors.toList());
		
		final int PB_INTERVAL = 10;
		final double PB_DEVIATION = 0.1;
		final int BREAKOUT_CONFIDENCE = 3;
		
		int buckets = period / PB_INTERVAL;
		int numberOfSkips = (int) (PB_INTERVAL * 1000 /  Constants.PERIOD_INTERVAL);
		double currentPrice = 0;
		double highestPrice = 0;
		double lowestPrice = 0;
		
		// Get highest and lowest price within given interval
		for (int i = 0; i <= ((buckets * numberOfSkips)-BREAKOUT_CONFIDENCE); i = i + numberOfSkips) {
			
			if (i==0) {
				highestPrice = stockPriceList.get(stockPriceList.size() - 1);
				lowestPrice = stockPriceList.get(stockPriceList.size() - 1);				
			}
			
			currentPrice = stockPriceList.get(stockPriceList.size() - 1 - i);
			if (currentPrice > highestPrice) highestPrice = currentPrice;
			if (currentPrice < lowestPrice) lowestPrice = highestPrice;
				
		}	
		
		// Set resistance and support level
		double resistance = highestPrice * (1 + PB_DEVIATION);
		double support = lowestPrice * (1 - PB_DEVIATION);
		
		// Check for Breakout opportunity
		int countResistence = 0;
		int countSupport = 0;
		
		for (int i = 0; i <= BREAKOUT_CONFIDENCE; i = i + 1) {

			currentPrice = stockPriceList.get(stockPriceList.size() - 1);
			
			if (currentPrice > resistance) countResistence += 1;
			else if (currentPrice < support) countSupport += 1;
		}
		
		if (countResistence == BREAKOUT_CONFIDENCE && !strategyOrder.getTradeOrder().getPosition().equals("short")) {
			return new Tuple<Boolean, String>(true, "short");
		}
		if (countSupport == BREAKOUT_CONFIDENCE  && !strategyOrder.getTradeOrder().getPosition().equals("long")) {
			return new Tuple<Boolean, String>(true, "long");
		}
			
		return new Tuple<Boolean, String>(false, null);
	}
}
