package com.fosd.app.entity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fosd.app.utilities.LocalDateTimeConverter;

@Entity(name="StrategyOrder")
@Table(name="strategy_order") 
public class StrategyOrder {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	private String name;
	private int sizeOrdered;
	@Convert(converter = LocalDateTimeConverter.class)
	private LocalDateTime timestamp;
	private String status;
	
	@ManyToOne
	@JoinColumn(name="stock_id")
	private Stock stock;

	@OneToMany(mappedBy="strategyOrder" , cascade= CascadeType.ALL, orphanRemoval=true)
	private List<TradeHistory> tradeHistorys = new ArrayList<TradeHistory>();
	
	@OneToOne(mappedBy="strategyOrder", cascade = CascadeType.ALL)
	private TradeOrder tradeOrder;
	
	public StrategyOrder() {}
		
	public StrategyOrder(String name, int sizeOrdered, LocalDateTime timestamp, String status) {
		this.name = name;
		this.sizeOrdered = sizeOrdered;
		this.timestamp = timestamp;
		this.status = status;
	}
	
	public void addTradeHistory(TradeHistory tradeHistory) {
		tradeHistorys.add(tradeHistory);
		tradeHistory.setStrategyOrder(this);
	}
	
	public void removeStrategyOrder(TradeHistory tradeHistory) {
		tradeHistorys.remove(tradeHistory);
		tradeHistory.setStrategyOrder(null);
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSizeOrdered() {
		return sizeOrdered;
	}

	public void setSizeOrdered(int sizeOrdered) {
		this.sizeOrdered = sizeOrdered;
	}

	public LocalDateTime getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(LocalDateTime timestamp) {
		this.timestamp = timestamp;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@JsonIgnore
	public Stock getStock() {
		return stock;
	}

	@JsonIgnore
	public void setStock(Stock stock) {
		this.stock = stock;
	}

	public TradeOrder getTradeOrder() {
		return tradeOrder;
	}

	public void setTradeOrder(TradeOrder tradeOrder) {
		this.tradeOrder = tradeOrder;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof StrategyOrder)) return false;
		return id == ((StrategyOrder) o).getId();
	}
	
	@Override
	public int hashCode() {
		return 31;
	}
}