package com.fosd.app.entity;

import java.time.LocalDateTime;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fosd.app.utilities.LocalDateTimeConverter;

@Entity(name="TradeHistory") 
@Table(name="trade_history") 
public class TradeHistory {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private boolean isLongPosition;
	private int sizeTraded;
	private double price;
	@Convert(converter = LocalDateTimeConverter.class)
	private LocalDateTime timestamp;
	private String result;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="strategy_order_id")
	private StrategyOrder strategyOrder;
	
	public TradeHistory() {}
	
	public TradeHistory(boolean isLongPosition, int sizeTraded, double price, LocalDateTime timestamp,
			String result) {
		this.isLongPosition = isLongPosition;
		this.sizeTraded = sizeTraded;
		this.price = price;
		this.timestamp = timestamp;
		this.result = result;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public boolean getIsLongPosition() {
		return isLongPosition;
	}

	public void setIsLongPosition(boolean isLongPosition) {
		this.isLongPosition = isLongPosition;
	}

	public int getSizeTraded() {
		return sizeTraded;
	}

	public void setSizeTraded(int sizeTraded) {
		this.sizeTraded = sizeTraded;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public LocalDateTime getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(LocalDateTime timestamp) {
		this.timestamp = timestamp;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	@JsonIgnore
	public StrategyOrder getStrategyOrder() {
		return strategyOrder;
	}

	@JsonIgnore
	public void setStrategyOrder(StrategyOrder strategyOrder) {
		this.strategyOrder = strategyOrder;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof TradeHistory)) return false;
		return id == ((TradeHistory) o).getId();
	}
	
	@Override
	public int hashCode() {
		return 31;
	}
}
