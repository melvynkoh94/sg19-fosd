package com.fosd.app.entity;

import java.time.LocalDateTime;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fosd.app.utilities.LocalDateTimeConverter;

@Entity(name = "StockPrice")
@Table(name="stock_price") 
public class StockPrice {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Convert(converter = LocalDateTimeConverter.class)
	private LocalDateTime timestamp;
	private double price;

	@ManyToOne()
	@JoinColumn(name="stock_id")
	private Stock stock;

	public StockPrice() {}

	public StockPrice(double price, LocalDateTime timestamp) {
		this.price =  price;
		this.timestamp = timestamp;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public LocalDateTime getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(LocalDateTime timestamp) {
		this.timestamp = timestamp;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	@JsonIgnore
	public Stock getStock() {
		return stock;
	}

	@JsonIgnore
	public void setStock(Stock stock) {
		this.stock = stock;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof StockPrice)) return false;
		return id == ((StockPrice) o).getId();
	}

	@Override
	public int hashCode() {
		return 31;
	}

	@Override
	public String toString() {
		return "Price is " + this.price + " for " + stock.getId() ;
	}
}
