package com.fosd.app.entity;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fosd.app.utilities.Constants;
import com.fosd.app.utilities.Tuple;

import jdk.internal.org.jline.utils.Log;

@Entity(name="TwoMovingAverageOrder") 
@Table(name="two_moving_average_order") 
public class TwoMovingAverageStrategy implements Strategy {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private int shortAvgWindow;
	private int longAvgWindow;
	private int period;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="strategy_order_id")
	private StrategyOrder strategyOrder;
	
	public TwoMovingAverageStrategy() {}
	
	public TwoMovingAverageStrategy(int shortWindow, int longWindow, int interval) {
		this.shortAvgWindow = shortWindow;
		this.longAvgWindow = longWindow;
		this.period = interval;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getShortAvgWindow() {
		return shortAvgWindow;
	}

	public void setShortAvgWindow(int shortAvgWindow) {
		this.shortAvgWindow = shortAvgWindow;
	}

	public int getLongAvgWindow() {
		return longAvgWindow;
	}

	public void setLongAvgWindow(int longAvgWindow) {
		this.longAvgWindow = longAvgWindow;
	}

	public int getPeriod() {
		return period;
	}

	public void setPeriod(int period) {
		this.period = period;
	}

	@Override
	public StrategyOrder getStrategyOrder() {
		return strategyOrder;
	}

	public void setStrategyOrder(StrategyOrder strategyOrder) {
		this.strategyOrder = strategyOrder;
	}

	@Override
	public Tuple<Boolean, String> doNextStep() {
		if (strategyOrder.getTradeOrder().getIsPartialFilled()) {
			return new Tuple<Boolean, String>(true,strategyOrder.getTradeOrder().getPosition());
		}		
		List<Double> stockPriceList = strategyOrder.getStock().getStockPrices().stream().map(x -> x.getPrice()).collect(Collectors.toList());

		int shortBuckets = shortAvgWindow / period;
		int longBuckets = longAvgWindow / period;

		double shortPrice = 0.0;
		double longPrice = 0.0;
		double shortPricePast = 0.0;
		double longPricePast = 0.0;
		double currentPrice = stockPriceList.get(stockPriceList.size() - 1);
		boolean isEntering = strategyOrder.getTradeOrder().getIsEntering();
		
		int numberOfSkips = (int) (period * 1000 / Constants.PERIOD_INTERVAL);
		int i = 0;
		int j = 0;
		for (i = 0; i < shortBuckets * numberOfSkips; i = i + numberOfSkips) {
			shortPrice += stockPriceList.get(stockPriceList.size() - 1 - i);
		}	
		shortPricePast = shortPrice - currentPrice + stockPriceList.get(stockPriceList.size() - 1 - i);
		longPrice = shortPrice;

		for (j = i; j < longBuckets * numberOfSkips; j = j + numberOfSkips) {
			longPrice += stockPriceList.get(stockPriceList.size() - 1 - j);
		}
		
		longPricePast = longPrice - currentPrice + stockPriceList.get(stockPriceList.size() - 1 - j);
		double avgLongPricePast = longPricePast / longBuckets;
		double avgShortPricePast = shortPricePast / shortBuckets;
		double avgLongPrice = longPrice / longBuckets;
		double avgShortPrice = shortPrice / shortBuckets;
		
		if (avgLongPricePast > avgShortPricePast && avgShortPrice > avgLongPrice) {
			if (strategyOrder.getTradeOrder().getPosition() != "long") {
				return new Tuple<Boolean, String> (true, "long");
			}
//			return (isEntering) ?  : (strategyOrder.getTradeOrder().getPosition()) new Tuple<Boolean, String> (true, "short");
		}
		if (avgLongPricePast < avgShortPricePast && avgShortPrice < avgLongPrice) {
			if (strategyOrder.getTradeOrder().getPosition() != "short") {
				return new Tuple<Boolean, String> (true, "short");
			}
//			return (isEntering) ? new Tuple<Boolean, String> (true, "short") : new Tuple<Boolean, String> (true, "long");
		}
		
		return new Tuple<Boolean, String>(false, null);
	}
}
