package com.fosd.app.entity;

public class CurrentHoldings {
	
	private TradeOrder tradeOrder;
	private StrategyOrder stratOrder; 
	private Stock stock;
	
	
	public CurrentHoldings(TradeOrder tradeOrder, StrategyOrder stratOrder, Stock stock) {
		this.tradeOrder = tradeOrder;
		this.stratOrder = stratOrder;
		this.stock = stock;
	}

	public TradeOrder getTradeOrder() {
		return tradeOrder;
	}
	
	public void setTradeOrder(TradeOrder tradeOrder) {
		this.tradeOrder = tradeOrder;
	}
	
	public StrategyOrder getStratOrder() {
		return stratOrder;
	}
	
	public void setStratOrder(StrategyOrder stratOrder) {
		this.stratOrder = stratOrder;
	}
	
	public Stock getStock() {
		return stock;
	}
	
	public void setStock(Stock stock) {
		this.stock = stock;
	} 	
}
