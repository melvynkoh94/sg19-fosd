package com.fosd.app;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.UUID;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.MessageType;

import com.fosd.app.entity.TradeHistory;
import com.fosd.app.entity.TradeOrder;
import com.fosd.app.mockOrderBroker.OrderBrokerSender;
import com.fosd.app.mockOrderBroker.RandomTradeOrderGenerator;
import com.fosd.app.service.FeedService;
import com.fosd.app.service.TradeHistoryService;
import com.fosd.app.service.TradeOrderService;

@EnableJms
@SpringBootApplication
@EnableJpaRepositories("com.fosd.app.repository")
public class AppApplication {

	public static void main(String[] args) {

		ApplicationContext ctx = SpringApplication.run(AppApplication.class, args);

		FeedService feedService = ctx.getBean(FeedService.class);
		feedService.start();

//		OrderBrokerSender sender = ctx.getBean(OrderBrokerSender.class);
//		String orderMessage = RandomTradeOrderGenerator.generateFixedTradeOrder();
//		String correlationID = UUID.randomUUID().toString();
//		System.out.println(correlationID.getBytes());
//		sender.sendOrderBrokerMessage(orderMessage, correlationID);
		
//		//For inserting mock data
//				TradeOrderService tradeOrderService = ctx.getBean(TradeOrderService.class);
//
//				ArrayList<TradeOrder> list = new ArrayList<TradeOrder>();
//				list.addAll(Arrays.asList(
//						new TradeOrder("closed", 500, "active", true, 0.0, false),
//						new TradeOrder("long", 200, "active", false, 1000, false),
//						new TradeOrder("short", 1000, "active", true, 3.6, false)
//						)
//			);
//				tradeOrderService.insertAllTradeOrders(list);
//
//
//		//For inserting mock data
//				TradeHistoryService tradeHistService = ctx.getBean(TradeHistoryService.class);
//
//				ArrayList<TradeHistory> tradeHistList = new ArrayList<TradeHistory>();
//				tradeHistList.addAll(Arrays.asList(
//						new TradeHistory(false, 500, 5.00, new GregorianCalendar(2019, Calendar.AUGUST, 20).getTime(), "F"),
//						new TradeHistory(false, 200, 5.30, new GregorianCalendar(2019, Calendar.AUGUST, 21).getTime(), "PF")
//						)		
//						);
//				tradeHistService.insertAllTradeHistory(tradeHistList);	
	}
	
	// This bean is necessary, to allow custom objects to be passed as message payloads (this converter converts objects to/from JSON).
	@Bean 
    public MessageConverter jacksonJmsMessageConverter() {
        MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
        converter.setTargetType(MessageType.TEXT);
        converter.setTypeIdPropertyName("_id");
        return converter;
    }
}

