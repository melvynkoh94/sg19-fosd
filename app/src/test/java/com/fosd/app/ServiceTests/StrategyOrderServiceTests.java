package com.fosd.app.ServiceTests;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;
import com.fosd.app.entity.StrategyOrder;
import com.fosd.app.repository.StrategyOrderRepository;
import com.fosd.app.service.StrategyOrderService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import static org.junit.Assert.assertEquals;



@RunWith(SpringRunner.class)
@SpringBootTest
public class StrategyOrderServiceTests {
	
	@MockBean
	private StrategyOrderRepository mockRepo;
	
	@Autowired
	StrategyOrderService service;
	
	
	@Test
	public void testSetStrategyOrder() {
		//StrategyOrder order = new StrategyOrder("TMA", x, 50, new GregorianCalendar(2019, Calendar.AUGUST, 20).getTime(), "Active");
		StrategyOrder order = new StrategyOrder();
		order.setStatus("active");
		order.setId(999);
		
		assertEquals(order.getStatus(), "active");
		
		order.setStatus("inactive");
		assertEquals(order.getStatus(), "inactive");

//		verify(mockRepo).findById(anyLong());
//		verify(mockRepo).update(anyLong(), refEq(order));		
	}
	
	
	
}
