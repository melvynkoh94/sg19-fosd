package com.fosd.app.RepositoryTests;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.fosd.app.entity.Stock;
import com.fosd.app.repository.StockRepository;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.refEq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
public class StockRepositoryTests {
	
	@Autowired
	private TestEntityManager em;
	
	@Autowired
	private StockRepository repository;
	
	@Test
	public void testFindStockByName() {
		em.persist(new Stock("Apple", "AAPL"));
		em.persist(new Stock("Google", "GOOG"));
		List<Stock> stocks = (List<Stock>) repository.findAll();
		assertEquals(2, stocks.size());
	}
	
	

}
