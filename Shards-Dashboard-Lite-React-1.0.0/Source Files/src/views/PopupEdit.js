import React, { Component } from "react";
import Popup from "../components/Popup/Popup";
import PopupEditParam from "../components/PopupEditParam/PopupEditParam";

class PopupEdit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showPopup: false
    };
  }

  togglePopup() {
    this.setState({
      showPopup: !this.state.showPopup
    });
  }

  handleSubmit() {
    let url = "http://app-fosd-project.dev2.conygre.com/updateTradeOrder/";
    url += this.props.strategyId;
    const body = JSON.stringify({
      id: this.props.strategyId,
      isActive: "Inactive"
    });
    console.log("exit strategy " + this.props.strategyId + url + body); //for checking
    // const strategyToExit = this.props.strategyId;

    fetch(url, {
      mode: "cors",
      method: "PUT",
      headers: {
        // "Access-Control-Allow-Origin": "*",
        Accept: "application/json, application/xml",
        "Content-Type": "application/json"
      },
      body: body
    })
      .then(response => {
        console.log(response);
        if (response.ok) {
          return response.json();
        } else {
          console.log("error");
          throw new Error("errorrrrr");
        }
      })
      .catch(error => this.setState({ error }));

    if (this.props.position === "Closed")
      return this.setState({
        showPopup: !this.state.showPopup,
        exitStatus: "Strategy Exited"
      });

    return this.setState({
      showPopup: !this.state.showPopup,
      exitStatus: "Exit Pending"
    });
  }

  getCorrectComopnent() {
    // dummy stragetyid first 1-TMA, 2-BB, 3-PB

    switch (this.props.strategy_name) {
      case "TMA":
        return (
          <PopupEditParam
            tradeOrderId={this.props.tradeOrderId}
            strategyId={this.props.strategyId}
            strategy_name={this.props.strategy_name}
            sizeToTrade={this.props.sizeToTrade}
            longAvg={this.props.longAvg}
            shortAvg={this.props.shortAvg}
            interval={this.props.interval}
            symbol={this.props.symbol}
            closePopup={this.togglePopup.bind(this)}
          />
        );
      case "BB":
        return (
          <PopupEditParam
            tradeOrderId={this.props.tradeOrderId}
            strategyId={this.props.strategyId}
            strategy_name={this.props.strategy_name}
            sizeToTrade={this.props.sizeToTrade}
            standardDev={this.props.standardDev}
            windowSize={this.props.windowSize}
            symbol={this.props.symbol}
            closePopup={this.togglePopup.bind(this)}
          />
        );
      case "PB":
        return (
          <PopupEditParam
            tradeOrderId={this.props.tradeOrderId}
            strategyId={this.props.strategyId}
            strategy_name={this.props.strategy_name}
            sizeToTrade={this.props.sizeToTrade}
            period={this.props.period}
            symbol={this.props.symbol}
            closePopup={this.togglePopup.bind(this)}
          />
        );
      default:
        return null;
    }
  }

  render() {
    return (
      <div>
        <button
          onClick={this.togglePopup.bind(this)}
          className="btn btn-dark btn-xl"
        >
          {this.props.strategyId}
        </button>

        {/* <span className="showStatus">{this.state.exitStatus}</span> */}

        {this.state.showPopup
          ? this.getCorrectComopnent()
          : // <PopupEditParam
            //   tradeOrderId={this.props.tradeOrderId}
            //   strategyId={this.props.strategyId}
            //   sizeToTrade={this.props.sizeToTrade}
            //   longAvg={this.props.longAvg}
            //   shortAvg={this.props.shortAvg}
            //   interval={this.props.interval}
            //   closePopup={this.togglePopup.bind(this)}
            // />
            null}

        <br />
      </div>
    );
  }
}

export default PopupEdit;
