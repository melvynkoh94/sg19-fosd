import React, { Component } from "react";
import Select from "react-select";
import Popup from "../components/Popup/Popup";
import PopupStocks from "./PopupStocks";
import {
  Container,
  Row,
  Col,
  Card,
  CardHeader,
  ListGroup,
  ListGroupItem,
  Form,
  Alert,
  FormGroup,
  FormInput,
  Button
} from "shards-react";
import { Tab } from "semantic-ui-react";
import PageTitle from "../components/common/PageTitle";

import TradingViewWidget, { Themes } from "react-tradingview-widget";
import { RealTimeChartWidget } from "react-tradingview-widgets";

import "semantic-ui-css/semantic.min.css";
import CanvasJSReact from "./canvasjs.react";
// import PopupStocks from "./PopupStocks";

var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

class ShakingError extends Component {
  constructor() {
    super();
    this.state = { key: 0 };
  }

  componentWillReceiveProps() {
    // update key to remount the component to rerun the animation
    this.setState({ key: ++this.state.key });
  }

  render() {
    return (
      <div key={this.state.key} className="bounce">
        {this.props.text}
      </div>
    );
  }
}
var dataPoints = [];
class ComponentsOverview extends Component {
  constructor(props) {
    super(props);
    // this.getDataReady = this.getDataReady.bind(this);

    this.handleSubmit_TMA = this.handleSubmit_TMA.bind(this);
    this.handleSubmit_BB = this.handleSubmit_BB.bind(this);
    this.handleSubmit_BP = this.handleSubmit_BP.bind(this);
    // this.generateDataPoints = this.generateDataPoints.bind(this);

    this.state = {
      check: null,
      data2: [],
      stock_name: this.props.stock_name
    };
  }
  check() {
    console.log(this.props.location.state.stock_name);
  }

  togglePopup() {
    this.setState({
      showPopup: !this.state.showPopup,
      popUpType: "submitTMA"
    });
  }

  handleSubmit_TMA(event) {
    event.preventDefault();
    if (!event.target.checkValidity()) {
      this.setState({
        invalid: true,
        displayErrors: true
      });
      return;
    }
    const data = new FormData(event.target);

    this.setState({
      res: this.stringifyFormData(data),
      invalid: false,
      displayErrors: false
    });

    var dataObjToSend = JSON.parse(this.stringifyFormData(data));
    var dataForParam = JSON.parse(this.stringifyFormData(data));
    // var dataObjToSend = data;
    // var dataForParam = data;
    delete dataObjToSend["longAvg"];
    delete dataObjToSend["shortAvg"];
    delete dataObjToSend["interval"];
    delete dataObjToSend["symbol"];
    // dataObjToSend["stockId"] = this.props.location.state.stockId;
    dataObjToSend["timestamp"] = new Date();
    dataObjToSend["status"] = "Active";
    console.log(typeof dataObjToSend);
    console.log(dataObjToSend);

    let url =
      "http://app-fosd-project.dev2.conygre.com/TMAconfirm?longWindow=" +
      dataForParam["longAvg"] +
      "&shortWindow=" +
      dataForParam["shortAvg"] +
      "&interval=" +
      dataForParam["interval"] +
      "&symbol=" +
      dataForParam["symbol"];

    console.log("data to send");
    console.log(JSON.stringify(dataObjToSend));
    const body = dataObjToSend;
    fetch(url, {
      method: "POST",
      headers: {
        // "Access-Control-Allow-Origin": "*",
        Accept: "application/json, application/xml",
        "Content-Type": "application/json"
      },
      body: JSON.stringify(body)
    })
      .then(response => {
        console.log(response);
        if (response.ok) {
          return response.json();
        } else {
          console.log("error");
          throw new Error("errorrrrr");
        }
      })
      .catch(error => this.setState({ error }));
  }

  handleSubmit_BB(event) {
    event.preventDefault();
    if (!event.target.checkValidity()) {
      this.setState({
        invalid: true,
        displayErrors: true
      });
      return;
    }
    const data = new FormData(event.target);

    this.setState({
      res: this.stringifyFormData(data),
      invalid: false,
      displayErrors: false
    });

    var dataObjToSend = JSON.parse(this.stringifyFormData(data));
    var dataForParam = JSON.parse(this.stringifyFormData(data));
    // var dataObjToSend = data;
    // var dataForParam = data;
    delete dataObjToSend["standardDeviation"];
    delete dataObjToSend["windowSize"];
    delete dataObjToSend["symbol"];

    // dataObjToSend["stockId"] = this.props.location.state.stockId;
    dataObjToSend["timestamp"] = new Date();
    dataObjToSend["status"] = "Active";
    console.log(typeof dataObjToSend);
    console.log(dataObjToSend);
    console.log(typeof dataForParam);
    console.log(dataForParam["longAvg"]);
    let url =
      "http://app-fosd-project.dev2.conygre.com/BBconfirm?standardDeviation=" +
      dataForParam["standardDeviation"] +
      "&windowSize=" +
      dataForParam["windowSize"] +
      "&symbol=" +
      dataForParam["symbol"];

    console.log("data to send");
    console.log(JSON.stringify(dataObjToSend));
    const body = dataObjToSend;
    fetch(url, {
      mode: "cors",
      method: "POST",
      headers: {
        // "Access-Control-Allow-Origin": "*",
        Accept: "application/json, application/xml",
        "Content-Type": "application/json"
      },
      body: JSON.stringify(body)
    })
      .then(response => {
        console.log(response);
        if (response.ok) {
          return response.json();
        } else {
          console.log("error");
          throw new Error("errorrrrr");
        }
      })
      .catch(error => this.setState({ error }));
  }

  handleSubmit_BP(event) {
    event.preventDefault();
    if (!event.target.checkValidity()) {
      this.setState({
        invalid: true,
        displayErrors: true
      });
      return;
    }
    const data = new FormData(event.target);

    this.setState({
      res: this.stringifyFormData(data),
      invalid: false,
      displayErrors: false
    });

    var dataObjToSend = JSON.parse(this.stringifyFormData(data));
    var dataForParam = JSON.parse(this.stringifyFormData(data));
    // var dataObjToSend = data;
    // var dataForParam = data;
    delete dataObjToSend["period"];
    delete dataObjToSend["symbol"];

    // dataObjToSend["stockId"] = this.props.location.state.stockId;
    dataObjToSend["timestamp"] = new Date();
    dataObjToSend["status"] = "Active";
    console.log(typeof dataObjToSend);
    console.log(dataObjToSend);
    console.log(typeof dataForParam);
    console.log(dataForParam["longAvg"]);
    let url =
      "http://app-fosd-project.dev2.conygre.com/BPconfirm?period=" +
      dataForParam["period"] +
      "&symbol=" +
      dataForParam["symbol"];

    console.log("data to send");
    console.log(JSON.stringify(dataObjToSend));
    const body = dataObjToSend;
    fetch(url, {
      mode: "cors",
      method: "POST",
      headers: {
        // "Access-Control-Allow-Origin": "*",
        Accept: "application/json, application/xml",
        "Content-Type": "application/json"
      },
      body: JSON.stringify(body)
    })
      .then(response => {
        console.log(response);
        if (response.ok) {
          return response.json();
        } else {
          console.log("error");
          throw new Error("errorrrrr");
        }
      })
      .catch(error => this.setState({ error }));
  }

  stringifyFormData(fd) {
    const data = {};
    // const myform = {};
    for (let key of fd.keys()) {
      data[key] = fd.get(key);
      // myform[key] = fd.get(key);
    }
    // console.log(myform);
    return JSON.stringify(data, null, 2);
  }

  componentDidMount() {
    var chart = this.chart;
    const stName = this.props.location.state.stock_symbol;
    var link =
      "http://feed2.conygre.com/API/StockFeed/GetStockPricesForSymbol/" +
      stName +
      "?HowManyValues=500";

    fetch(link)
      .then(response => response.json())
      .then(function(data) {
        for (var i = 0; i < data.length; i++) {
          var x_data = data[i].tradeTime.split(":").join("");
          dataPoints.push({
            // x: new Date(data[i].periodNumber),

            label: data[i].tradeTime,
            y: data[i].Price
          });
        }
        chart.render();
      });
  }

  // handleSubmit() {
  //   let url = "http://localhost:8080/home/updateTradeOrder/";
  //   url += this.props.strategyId;
  //   const body = JSON.stringify({
  //     id: this.props.strategyId,
  //     isActive: "Inactive"
  //   });
  //   console.log("exit strategy " + this.props.strategyId + url + body); //for checking
  //   // const strategyToExit = this.props.strategyId;

  //   fetch(url, {
  //     mode: "cors",
  //     method: "PUT",
  //     headers: {
  //       // "Access-Control-Allow-Origin": "*",
  //       Accept: "application/json, application/xml",
  //       "Content-Type": "application/json"
  //     },
  //     body: body
  //   })
  //     .then(response => {
  //       console.log(response);
  //       if (response.ok) {
  //         return response.json();
  //       } else {
  //         console.log("error");
  //         throw new Error("errorrrrr");
  //       }
  //     })
  //     .catch(error => this.setState({ error }));

  //   if (this.props.position === "Closed")
  //     return this.setState({
  //       showPopup: !this.state.showPopup,
  //       exitStatus: "Strategy Exited"
  //     });

  //   return this.setState({
  //     showPopup: !this.state.showPopup,
  //     exitStatus: "Exit Pending"
  //   });
  // }

  render() {
    const chart_css = {
      theme: "light1", // "light1", "dark1", "dark2"
      animationEnabled: true,
      zoomEnabled: true,
      title: {
        text: "Latest stock price"
      },
      axisY: {
        includeZero: false
      },
      axisX: {
        // valueFormatString: "hh:mm:ss",
        labelAngle: 50
      },
      data: [
        {
          type: "area",
          // xValueType: "dateTime",
          // dataPoints: this.generateDataPoints(500)
          dataPoints: dataPoints
        }
      ]
    };

    const { res, invalid, displayErrors, data } = this.state;

    const Tab_styles = {
      fontSize: 100,
      fontWeight: "bold"
    };

    const panes = [
      {
        menuItem: "Two Moving Averages",
        render: () => (
          <Tab.Pane>
            <ListGroup flush>
              <ListGroupItem className="p-3">
                <form onSubmit={this.handleSubmit_TMA}>
                  <Row>
                    <Col align="right">
                      <FormGroup>
                        {/* pass stock name into {} */}
                        <label style={{ color: "black" }} htmlFor="stock">
                          Stock:{" "}
                        </label>
                      </FormGroup>
                      <FormGroup>
                        {/* pass price into {} */}
                        <label style={{ color: "black" }} htmlFor="stock">
                          Current Price:
                        </label>
                      </FormGroup>
                      <FormGroup>
                        <label style={{ color: "black" }} htmlFor="quantity">
                          Size to buy:
                        </label>
                      </FormGroup>
                      <FormGroup>
                        <label style={{ color: "black" }} htmlFor="long_avg">
                          Long Average:
                        </label>
                      </FormGroup>
                      <FormGroup>
                        <label style={{ color: "black" }} htmlFor="short_avg">
                          Short Average:
                        </label>
                      </FormGroup>
                      <FormGroup>
                        <label style={{ color: "black" }} htmlFor="short_avg">
                          Interval:
                          <br />
                          (no. of minutes)
                        </label>
                      </FormGroup>
                    </Col>
                    <Col>
                      <FormGroup>
                        <label style={{ color: "black" }} htmlFor="stock_name">
                          {this.props.location.state.stock_name}
                        </label>
                      </FormGroup>
                      <FormGroup>
                        <label style={{ color: "black" }} htmlFor="stock_price">
                          {this.props.location.state.stock_price}
                        </label>
                      </FormGroup>
                      <FormGroup>
                        <input
                          type="hidden"
                          id="name"
                          name="name"
                          value="TMA"
                        />
                      </FormGroup>
                      <FormGroup>
                        <input
                          type="hidden"
                          id="symbol"
                          name="symbol"
                          value={this.props.location.state.stock_symbol}
                        />
                      </FormGroup>
                      <FormGroup>
                        <FormInput
                          id="sizeOrdered"
                          name="sizeOrdered"
                          placeholder="Minimum 1"
                          type="number"
                          required
                          min={1}
                        />
                      </FormGroup>

                      <FormGroup>
                        <FormInput
                          id="longAvg"
                          name="longAvg"
                          placeholder="Value from 1 to 30"
                          type="number"
                          required
                          min={1}
                          max={30}
                        />
                      </FormGroup>
                      <FormGroup>
                        <FormInput
                          id="shortAvg"
                          name="shortAvg"
                          placeholder="Value from 1 to 30"
                          type="number"
                          required
                          min={1}
                          max={30}
                        />
                      </FormGroup>
                      <FormGroup>
                        <FormInput
                          id="interval"
                          name="interval"
                          placeholder="Minimum 1"
                          type="number"
                          required
                        />
                      </FormGroup>
                    </Col>
                  </Row>

                  <Row className="mt-4">
                    <Col>
                      <div align="right">
                        {/* <p>{this.state.data2}</p> */}
                        <Button
                          outline
                          size="lg"
                          className="mb-2 mx-4"
                          id="2MA"
                          name="2MA"
                        >
                          Submit 2MA
                        </Button>
                        {/* <PopupStocks value={this.state.data2} /> */}

                        {/* </Button> */}
                      </div>
                    </Col>
                  </Row>
                </form>

                {/* <div className="res-block">
                  {invalid && <ShakingError text="Form is not valid" />}
                  {!invalid && res && (
                    <div>
                      <h3>Transformed data to be sent:</h3>
                      <pre>FormData {res}</pre>
                      <h1>{this.state.data2}</h1>
                    </div>
                  )}
                </div> */}
              </ListGroupItem>
            </ListGroup>
          </Tab.Pane>
        )
      },
      //####################################################################################################
      {
        menuItem: "Bollinger Bands",
        render: () => (
          <Tab.Pane>
            <ListGroup flush>
              <ListGroupItem className="p-3">
                <form onSubmit={this.handleSubmit_BB}>
                  <Row>
                    <Col align="right">
                      <FormGroup>
                        {/* pass stock name into {} */}
                        <label style={{ color: "black" }} htmlFor="stock">
                          Stock:{" "}
                        </label>
                      </FormGroup>
                      <FormGroup>
                        {/* pass price into {} */}
                        <label style={{ color: "black" }} htmlFor="stock">
                          Current Price:
                        </label>
                        <FormGroup>
                          <label style={{ color: "black" }} htmlFor="quantity">
                            Size to buy:
                          </label>
                        </FormGroup>
                      </FormGroup>
                      <FormGroup>
                        <label style={{ color: "black" }} htmlFor="quantity">
                          Standard Deviation:
                        </label>
                      </FormGroup>
                      <FormGroup>
                        <label style={{ color: "black" }} htmlFor="long_avg">
                          WindowSize:
                        </label>
                      </FormGroup>
                    </Col>
                    <Col>
                      <FormGroup>
                        <label style={{ color: "black" }} htmlFor="stock">
                          {this.props.location.state.stock_name}
                        </label>
                      </FormGroup>
                      <FormGroup>
                        <label style={{ color: "black" }} htmlFor="stock">
                          {this.props.location.state.stock_price}
                        </label>
                      </FormGroup>
                      <FormGroup>
                        <FormInput
                          id="standardDeviation"
                          name="standardDeviation"
                          placeholder="Minimum 1"
                          min={1}
                        />
                      </FormGroup>

                      <FormGroup>
                        <FormInput
                          id="windowSize"
                          name="windowSize"
                          placeholder="Minimum 1"
                          min={1}
                        />
                      </FormGroup>
                      <FormGroup>
                        <input type="hidden" id="name" name="name" value="BB" />
                      </FormGroup>
                      <FormGroup>
                        <input
                          type="hidden"
                          id="symbol"
                          name="symbol"
                          value={this.props.location.state.stock_symbol}
                        />
                      </FormGroup>

                      <FormGroup>
                        <FormInput
                          id="sizeOrdered"
                          name="sizeOrdered"
                          placeholder="Minimum 1"
                          type="number"
                          required
                          min={1}
                        />
                      </FormGroup>
                    </Col>
                  </Row>

                  <Row className="mt-4">
                    <Col>
                      <div align="right">
                        <Button
                          outline
                          size="lg"
                          className="mb-2 mx-4"
                          id="BB"
                          name="BB"
                        >
                          Submit BB
                        </Button>
                      </div>
                    </Col>
                  </Row>
                </form>

                {/* <div className="res-block">
                  {invalid && <ShakingError text="Form is not valid" />}
                  {!invalid && res && (
                    <div>
                      <h3>Transformed data to be sent:</h3>
                      <pre>FormData {res}</pre>
                    </div>
                  )}
                </div> */}
              </ListGroupItem>
            </ListGroup>
          </Tab.Pane>
        )
      },

      {
        menuItem: "Simple Price Breakout",
        render: () => (
          <Tab.Pane>
            <ListGroup flush>
              <ListGroupItem className="p-3">
                <form onSubmit={this.handleSubmit_BP}>
                  <Row>
                    <Col align="right">
                      <FormGroup>
                        {/* pass stock name into {} */}
                        <label style={{ color: "black" }} htmlFor="stock">
                          Stock:{" "}
                        </label>
                      </FormGroup>
                      <FormGroup>
                        {/* pass price into {} */}
                        <label style={{ color: "black" }} htmlFor="price">
                          Current Price:
                        </label>
                      </FormGroup>
                      <FormGroup>
                        <label style={{ color: "black" }} htmlFor="quantity">
                          Size to buy:
                        </label>
                      </FormGroup>
                      <FormGroup>
                        <label style={{ color: "black" }} htmlFor="quantity">
                          Period:
                        </label>
                      </FormGroup>
                    </Col>
                    <Col>
                      <FormGroup>
                        <label style={{ color: "black" }} htmlFor="stock">
                          {this.props.location.state.stock_name}
                        </label>
                      </FormGroup>
                      <FormGroup>
                        <label style={{ color: "black" }} htmlFor="price">
                          {this.props.location.state.stock_price}
                        </label>
                      </FormGroup>
                      <FormGroup>
                        <FormInput
                          id="sizeOrdered"
                          name="sizeOrdered"
                          placeholder="Minimum 1"
                          type="number"
                          required
                          min={1}
                        />
                      </FormGroup>
                      <FormGroup>
                        <FormInput
                          id="period"
                          name="period"
                          placeholder="Minimum 5 min"
                          min={5}
                        />
                      </FormGroup>

                      <FormGroup>
                        <input
                          type="hidden"
                          id="name"
                          name="name"
                          value="SBP"
                        />
                      </FormGroup>
                      <FormGroup>
                        <input
                          type="hidden"
                          id="symbol"
                          name="symbol"
                          value={this.props.location.state.stock_symbol}
                        />
                      </FormGroup>
                    </Col>
                  </Row>

                  <Row className="mt-4">
                    <Col>
                      <div align="right">
                        <Button
                          outline
                          size="lg"
                          className="mb-2 mx-4"
                          id="SBP"
                          name="SBP"
                        >
                          Submit SBP
                        </Button>
                      </div>
                    </Col>
                  </Row>
                </form>

                {/* <div className="res-block">
                  {invalid && <ShakingError text="Form is not valid" />}
                  {!invalid && res && (
                    <div>
                      <h3>Transformed data to be sent:</h3>
                      <pre>FormData {res}</pre>
                    </div>
                  )}
                </div> */}
              </ListGroupItem>
            </ListGroup>
          </Tab.Pane>
        )
      }
    ];

    return (
      <div>
        <Container fluid className="main-content-container px-4">
          <Row noGutters className="page-header py-4">
            <PageTitle
              sm="4"
              title="Forms & Components"
              subtitle="Overview"
              className="text-sm-left"
            />
          </Row>
          <Row>
            <Col md="6" className="mb-4">
              {/* Complete Form Example */}
              <Card small>
                <CardHeader className="border-bottom">
                  <h6 className="m-0">Let's Get Started!</h6>
                </CardHeader>

                <Tab
                  menu={{
                    fluid: true,
                    vertical: true,
                    tabular: true,
                    color: "blue"
                  }}
                  panes={panes}
                />
              </Card>
            </Col>
            <Col>
              <div>
                <CanvasJSChart
                  options={chart_css}
                  onRef={ref => (this.chart = ref)}
                  /* onRef = {ref => this.chart = ref} */
                />
              </div>
              {/* <RealTimeChartWidget
                symbol={this.props.location.state.stock_symbol}
                locale="en"
                autosize
                interval="D"
              /> */}
            </Col>

            {/* {this.state.showPopup ? (
              <Popup
                closePopup={this.togglePopup.bind(this)}
                // submitPopup={this.handleSubmit_TMA.bind(this)}
                submitPopup={this.handleSubmit_TMA.bind(this)}
                popUpType={this.state.popUpType}
                longAvg={this.state.longAvg}
              />
            ) : null} */}

            {/* <Col>
              <div class="tradingview-widget-container">
                <div id="tradingview_b3c56" />
                <div class="tradingview-widget-copyright">
                  <a
                    href="https://www.tradingview.com/symbols/NASDAQ-AAPL/"
                    rel="noopener"
                    target="_blank"
                  >
                    <span class="blue-text">AAPL Chart</span>
                  </a>{" "}
                  by TradingView
                </div>
                <script
                  type="text/javascript"
                  src="https://s3.tradingview.com/tv.js"
                />
                new TradingView.widget(
  {
  "autosize": true,
  "symbol": "NASDAQ:AAPL",
  "interval": "1",
  "timezone": "Asia/Singapore",
  "theme": "Light",
  "style": "1",
  "locale": "en",
  "toolbar_bg": "#f1f3f6",
  "enable_publishing": false,
  "allow_symbol_change": true,
  "container_id": "tradingview_b3c56"
}
  );
                <script type="text/javascript" />
              </div>
            </Col> */}
          </Row>
        </Container>
      </div>
    );
  }
}

export default ComponentsOverview;
