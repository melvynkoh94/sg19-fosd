import React, { Component } from "react";
import { Row, Col, Card, CardHeader, CardBody } from "shards-react";
import Pagination from "./Pagination";
import "./pagination.css";

class PaginationTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      historyTable: this.props.historyTable,
      pageOfItems: []
    };
    this.onChangePage = this.onChangePage.bind(this);
  }
  //longposition:true = long
  checkPosition = position => {
    if (position === true) return "Long";
    return "Short";
  };
  calPerf = (price, size, position) => {
    if (position) {
      return price * size * -1;
    }
    if (position) {
      return price * size;
    }
  };

  checkPerformancePositive(func) {
    let ans = func();
    if (ans > 0) {
      return "green";
    } else {
      return "red";
    }
  }
  onChangePage(pageOfItems) {
    // update state with new page of items
    this.setState({ pageOfItems: pageOfItems });
  }

  render() {
    return (
      <div>
        <Row>
          <Col>
            <Card small className="mb-4 overflow-hidden">
              <CardHeader className="bg-dark border-bottom">
                <h6 className="m-0 text-white">Trade History</h6>
              </CardHeader>
              <CardBody className="bg-dark p-0 pb-3">
                <table className="table table-dark mb-0">
                  <thead className="thead-dark">
                    <tr>
                      <th scope="col" className="border-0">
                        #
                      </th>
                      <th scope="col" className="border-0">
                        Date
                      </th>
                      <th scope="col" className="border-0">
                        Strategy
                      </th>
                      <th scope="col" className="border-0">
                        Stock Name
                      </th>
                      <th scope="col" className="border-0">
                        Stock Symbol
                      </th>
                      <th scope="col" className="border-0">
                        Position
                      </th>
                      <th scope="col" className="border-0">
                        Price Traded
                      </th>
                      <th scope="col" className="border-0">
                        Size
                      </th>
                      <th scope="col" className="border-0">
                        Performance
                      </th>
                      {/* <th scope="col" className="border-0">
                        Status
                      </th> */}
                    </tr>
                  </thead>
                  <tbody>
                    {/* taking each object and map it to a table row */}
                    {this.state.pageOfItems.map(trade => (
                      <tr key={trade.trade_id}>
                        <td>{trade.stategy_order_id}</td>
                        <td>{trade.trade_timestamp}</td>
                        <td>{trade.stategy_name}</td>
                        <td>{trade.stock_name}</td>
                        <td>{trade.stock_symbol}</td>

                        <td>{this.checkPosition(trade.trade_position)}</td>

                        <td>{trade.trade_price}</td>
                        <td>{trade.trade_sizeTraded}</td>

                        {/* <td>{trade.result}</td> */}
                        <td
                          style={{
                            color: this.checkPerformancePositive(this.calPerf)
                          }}
                        >
                          {this.calPerf(
                            trade.trade_price,
                            trade.trade_sizeTraded,
                            trade.trade_isBuy
                          )}
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </CardBody>
            </Card>
          </Col>
        </Row>

        <div>
          <Pagination
            className="pagination"
            items={this.props.historyTable}
            onChangePage={this.onChangePage}
          />
        </div>
      </div>
    );
  }
}

export default PaginationTable;
