import React, { Component } from "react";
import { Row, Col, Card, CardHeader, CardBody, Button } from "shards-react";
import Pagination from "./Pagination";
import "./pagination.css";

import { Link } from "react-router-dom";
var stockTable = [];
class PaginationTable_stocks extends Component {
  constructor(props) {
    super(props);
    // this.handleSubmit = this.handleSubmit.bind(this);

    this.state = {
      tableToPass: this.props.tableToPass,
      // priceTable: [],
      pageOfItems: []
    };
    this.onChangePage = this.onChangePage.bind(this);
  }
  //longposition:true = long
  checkPosition = position => {
    if (position === true) return "Long";
    return "Short";
  };

  onChangePage(pageOfItems) {
    // update state with new page of items
    this.setState({ pageOfItems: pageOfItems });
  }

  // handleSubmit(stock) {
  //   console.log("hellohello");
  //   console.log(stock.stock_name + stock.stock_price + stock.stock_symbol);
  //   console.log(this.props.history);

  //   this.props.history.push({
  //     pathname: "/submitStrategyOrder",
  //     state: {
  //       stock_name: stock.stock_name,
  //       stock_price: stock.stock_price,
  //       stock_symbol: stock.stock_symbol
  //     }
  //   });
  // }

  componentDidMount() {
    // var chart = this.chart;
    // const stName = this.props.location.state.stock_symbol;

    fetch("http://app-fosd-project.dev2.conygre.com/displayAllStocks")
      .then(res => res.json())
      .then(data => {
        this.setState({
          pageOfItems: data.response,
          stockTable: data.response
        });
        console.log("new dataaa");
        console.log(this.state.pageOfItems);
      })
      .catch(console.log);
  }

  render() {
    // this.setState({ priceTable: priceTableOutside });
    return (
      <div>
        <Row>
          <Col md="6" className="makecenter">
            <Card small>
              <CardHeader className="border-bottom">
                <h6 className="m-0">Choose your stock</h6>
              </CardHeader>
              <CardBody className="p-0 pb-3">
                <table className="table mb-0" width="50%">
                  <thead className="bg-light">
                    <tr>
                      <th scope="col" className="border-0 pl-5">
                        Stock Name
                      </th>
                      <th scope="col" className="border-0">
                        Price
                      </th>
                    </tr>
                  </thead>

                  <tbody>
                    {/* taking each object and map it to a table row */}
                    {this.state.pageOfItems.map(stock => (
                      <tr key={stock.id}>
                        <td className="pl-5">
                          <Link
                            to={{
                              pathname: "/submitStrategyOrder",
                              state: {
                                stock_name: stock.stock_name,
                                stock_price: stock.stock_price_latest,
                                stock_symbol: stock.stock_symbol,
                                stockId: stock.stock_id
                              }
                            }}
                          >
                            {stock.stock_name}
                          </Link>
                        </td>
                        <td>{stock.stock_price_latest}</td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <div>
          <Pagination
            className="pagination"
            items={this.state.stockTable}
            onChangePage={this.onChangePage}
          />
        </div>
      </div>
    );
  }
}

export default PaginationTable_stocks;
