import React, { Component } from "react";
import { Container, Button } from "shards-react";

class TMAconfirm extends Component {
  constructor() {
    super();
  }
  showDetails() {
    console.log("check details to send to backend");
    console.log(this.props.location.state.data);
  }

  render() {
    return (
      <Container fluid className="main-content-container px-4 pb-4">
        <div className="error">
          <div className="error__content">
            <h2>✅ </h2>
            <h3>
              Your strategy (Two Moving Averages) has been successfully
              submitted.
            </h3>
            {/* <p>There was a problem on our end. Please try again later.</p> */}
            <Button onClick={() => this.showDetails()}>Show Details</Button>
            <br />
            <Button pill>&larr; Home</Button>
          </div>
        </div>
      </Container>
    );
  }
}

export default TMAconfirm;
