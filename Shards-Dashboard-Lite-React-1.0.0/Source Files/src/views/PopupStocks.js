import React, { Component } from "react";
import Popup from "../components/Popup/Popup";

class PopupStocks extends Component {
  constructor(props) {
    super(props);
    this.state = {
      //   popUpType: "exitStrat",
      value: this.props.value,
      showPopup: false
    };
  }

  togglePopup() {
    this.setState({
      showPopup: !this.state.showPopup
    });
  }

  handleSubmit() {
    console.log(this.props.value);
    // let url = "http://localhost:8080/home/updateTradeOrder/";
    // url += this.props.strategyId;
    // const body = JSON.stringify({
    //   id: this.props.strategyId,
    //   isActive: "Inactive"
    // });
    // console.log("exit strategy " + this.props.strategyId + url + body); //for checking
    // // const strategyToExit = this.props.strategyId;
    // fetch(url, {
    //   mode: "cors",
    //   method: "PUT",
    //   headers: {
    //     // "Access-Control-Allow-Origin": "*",
    //     Accept: "application/json, application/xml",
    //     "Content-Type": "application/json"
    //   },
    //   body: body
    // })
    //   .then(response => {
    //     console.log(response);
    //     if (response.ok) {
    //       return response.json();
    //     } else {
    //       console.log("error");
    //       throw new Error("errorrrrr");
    //     }
    //   })
    //   .catch(error => this.setState({ error }));
    // if (this.props.position === "Closed")
    //   return this.setState({
    //     showPopup: !this.state.showPopup,
    //     exitStatus: "Strategy Exited"
    //   });
    // return this.setState({
    //   showPopup: !this.state.showPopup,
    //   exitStatus: "Exit Pending"
    // });
  }

  render() {
    return (
      <div>
        <button
          onClick={this.togglePopup.bind(this)}
          className="btn btn-dark btn-xl"
        >
          Submit
        </button>

        {/* <span className="showStatus">{this.state.exitStatus}</span> */}

        {this.state.showPopup ? (
          <Popup
            closePopup={this.togglePopup.bind(this)}
            submitPopup={this.handleSubmit.bind(this)}
            strategyId={this.props.strategyId}
            popUpType={this.state.popUpType}
          />
        ) : null}

        <br />
      </div>
    );
  }
}

export default PopupStocks;
