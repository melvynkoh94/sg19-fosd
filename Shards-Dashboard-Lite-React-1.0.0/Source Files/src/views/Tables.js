import React, { Component } from "react";
import { Container, Row, Col, Card, CardHeader, CardBody } from "shards-react";

import PageTitle from "../components/common/PageTitle";

import TableData from "./TableData";
import TradeHistory from "./TradeHistory";
import AnimatedNumber from "react-animated-number";

const investment = {
  count: -417.0 //change accordingly
};

class Tables extends Component {
  constructor(props) {
    super(props);
    // this.state = {
    //   strategy_name: "SPB"
    //
    // };
  }
  state = {
    profit: []
  };

  componentDidMount() {
    fetch("http://app-fosd-project.dev2.conygre.com/totalProfits")
      .then(res => res.json())
      .then(data => {
        this.setState({ profit: data });
        console.log("profit below");
        console.log(this.state.profit["response"]);
      })
      .catch(console.log);
  }

  render() {
    return (
      <div>
        <Container fluid className="main-content-container px-4">
          {/* Page Header */}
          <Row noGutters className="page-header py-4">
            <PageTitle
              sm="4"
              title="WELCOME TRADER"
              subtitle="CITI'S FOSD PROJECT"
              className="text-sm-left"
            />
          </Row>

          {/* Return trader's current profit/loss */}
          <div align="center" className="pt-2 pb-5">
            <h6>Return of Investments:</h6>
            <h1 className="pb-5" style={{ fontSize: 70 }}>
              {/* ${investment.count.toFixed(2)} */}
              <AnimatedNumber
                component="text"
                value={Number(this.state.profit["response"]).toFixed(2)}
                style={{
                  transition: "0.8s ease-out",
                  fontSize: 100,
                  transitionProperty: "background-color, color, opacity"
                }}
                frameStyle={perc =>
                  perc === 100 ? {} : { backgroundColor: "" }
                }
                duration={600}
                // formatValue={n => prettyBytes(n)}
              />
            </h1>
            <a href="blog-posts">
              <button
                className="btn btn-secondary btn-xl"
                style={{ fontSize: 20 }}
              >
                Start Trading
              </button>
            </a>
          </div>
          {/* Trade History Table */}
          <TradeHistory />
          {/* Current Holding Table */}
          <TableData />
        </Container>
      </div>
    );
  }
}

export default Tables;
