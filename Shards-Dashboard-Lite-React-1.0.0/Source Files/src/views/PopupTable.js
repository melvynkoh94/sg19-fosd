import React, { Component } from "react";
import Popup from "../components/Popup/Popup";

class PopupTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      popUpType: "exitStrat",
      showPopup: false
    };
  }

  togglePopup() {
    this.setState({
      showPopup: !this.state.showPopup
    });
  }

  handleSubmit() {
    const trade_id = this.props.trade_id;
    let url =
      "http://192.168.8.198:8080/updateTradeOrder/" +
      trade_id +
      "?status=inactive";
    console.log("url below");
    console.log(url);
    // const body = JSON.stringify({
    //   id: this.props.strategyId,
    //   status: "Inactive"
    // });
    // console.log("exit strategy " + this.props.strategyId + url + body); //for checking
    // const strategyToExit = this.props.strategyId;

    fetch(url, {
      mode: "cors",
      method: "PUT",
      headers: {
        // "Access-Control-Allow-Origin": "*",
        Accept: "application/json, application/xml",
        "Content-Type": "application/json"
      }
      // body: body
    })
      .then(response => {
        console.log(response);
        if (response.ok) {
          return response.json();
        } else {
          console.log("error");
          throw new Error("errorrrrr");
        }
      })
      .catch(error => this.setState({ error }));

    if (this.props.status === "closed")
      this.setState({
        showPopup: !this.state.showPopup,
        exitStatus: "Strategy Exited"
      });
    else {
      this.setState({
        showPopup: !this.state.showPopup,
        exitStatus: "Exit Pending"
      });
    }
  }

  decideAction(params) {
    if (params === "Exit") {
      this.setState({
        popUpType: "exitStrat"
      });
    } else if (params === "Pause") {
      this.setState({
        popUpType: "pause"
      });
    }

    return params;
  }

  exitOrPause() {
    if (this.props.word === "exitStrat") {
      return "Exit";
    } else {
      return "Pause";
    }
  }
  render() {
    return (
      <div>
        <button
          onClick={this.togglePopup.bind(this)}
          className="btn btn-dark btn-xl"
        >
          {this.exitOrPause()}
        </button>

        <span className="showStatus">{this.state.exitStatus}</span>

        {this.state.showPopup ? (
          <Popup
            closePopup={this.togglePopup.bind(this)}
            submitPopup={this.handleSubmit.bind(this)}
            strategyId={this.props.strategyId}
            popUpType={this.props.word}
          />
        ) : null}

        <br />
      </div>
    );
  }
}

export default PopupTable;
