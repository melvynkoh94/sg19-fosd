import React, { Component } from "react";
import { Container, Row, Col, Card, CardHeader, CardBody } from "shards-react";
import PopupTable from "./PopupTable";
import Popup from "../components/Popup/Popup";
import PopupEdit from "./PopupEdit";
import Pagination from "../views/Pagination/Pagination";

class TableData extends Component {
  constructor(props) {
    super(props);
    this.state = {
      strategy_name: "PB",
      currentHoldingsTable: []
      // showPopup: false
    };
    this.onChangePage = this.onChangePage.bind(this);
  }
  // state = {
  //   currentHoldingsTable: [],
  //   exitStatus: ""
  // };

  //entering:true = closed?
  checkPosition = position => {
    if (position === true) return "Closed";
    return "Open";
  };
  onChangePage(currentHoldingsTable) {
    // update state with new page of items
    this.setState({ currentHoldingsTable: currentHoldingsTable });
  }

  //If performance is positive, appear in green. else, show in red
  checkPerformancePositive = perf => {
    let color = "";
    color += perf > 0 ? "green" : "red";
    return color;
  };

  componentDidMount() {
    fetch("http://app-fosd-project.dev2.conygre.com/tradeOrders")
      .then(res => res.json())
      .then(data => {
        this.setState({ currentHoldingsTable: data.response });
        console.log("tradeOrder data fetched");
        console.log(this.state.currentHoldingsTable);
      })
      .catch(console.log);
  }
  togglePopup() {
    this.setState({
      showPopup: !this.state.showPopup
    });
  }

  getCorrectComopent(
    strategy_name,
    strategy_id,
    strategy_order_id,
    stock_symbol
  ) {
    // dummy stragetyid first 1-TMA, 2-BB, 3-PB
    const tradeOrderId = "001";
    // const strategyId = "1";
    // const strategy_name = "PB";
    const sizeToTrade = "500";
    const longAvg = "5";
    const shortAvg = "3";
    const interval = "10";

    const standardDev = "3";
    const windowSize = "4";

    const period = "100";

    switch (strategy_name) {
      case "TMA":
        return (
          <PopupEdit
            tradeOrderId={tradeOrderId}
            strategyId={strategy_order_id}
            strategy_name={strategy_name}
            sizeToTrade={sizeToTrade}
            longAvg={longAvg}
            shortAvg={shortAvg}
            interval={interval}
            symbol={stock_symbol}
          />
        );
      case "BB":
        return (
          <PopupEdit
            tradeOrderId={tradeOrderId}
            strategyId={strategy_order_id}
            strategy_name={strategy_name}
            sizeToTrade={sizeToTrade}
            standardDev={standardDev}
            windowSize={windowSize}
            symbol={stock_symbol}
          />
        );
      case "PB":
        return (
          <PopupEdit
            tradeOrderId={tradeOrderId}
            strategyId={strategy_order_id}
            strategy_name={strategy_name}
            sizeToTrade={sizeToTrade}
            period={period}
            symbol={stock_symbol}
          />
        );
      default:
        return null;
    }
  }

  render() {
    return (
      <div>
        <Row>
          <Col>
            <Card small className="mb-4">
              <CardHeader className="border-bottom">
                <h6 className="m-0">Current Holdings</h6>
              </CardHeader>
              <CardBody className="p-0 pb-3">
                <table className="table mb-0">
                  <thead className="bg-light">
                    <tr>
                      <th scope="col" className="border-0">
                        #
                      </th>
                      <th scope="col" className="border-0">
                        Strategy
                      </th>
                      <th scope="col" className="border-0">
                        Stock Name
                      </th>
                      <th scope="col" className="border-0">
                        Stock Symbol
                      </th>
                      <th scope="col" className="border-0">
                        Position
                      </th>
                      <th scope="col" className="border-0">
                        Price Traded
                      </th>
                      <th scope="col" className="border-0">
                        Size
                      </th>
                      <th scope="col" className="border-0">
                        Performance
                      </th>
                      <th scope="col" className="border-0" />
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.currentHoldingsTable.map(trade => (
                      <tr key="tradeId">
                        <td>
                          {/* make function here to decide which parameter to pass to another page */}
                          {/* this is a button */}
                          {/* maybe can pass all the parameters here then ignore if never being passed */}
                          {this.getCorrectComopent(
                            trade.strategy_name,
                            trade.strategy_id,
                            trade.strategy_order_id,
                            trade.stock_symbol
                          )}
                        </td>

                        {/* <td>{trade.strategy_id}</td> */}
                        <td>{trade.strategy_name}</td>
                        <td>{trade.stock_name}</td>
                        <td>{trade.stock_symbol}</td>
                        <td>{trade.trade_position}</td>
                        <td>{trade.base_price.toFixed(2)}</td>
                        <td>{trade.trade_sizeToTrade}</td>
                        <td>{trade.strategy_order_status}</td>

                        {/* <td>{this.state.strategy_name}</td>
                    <td>stockname</td>
                    <td>stocksymbol</td>
                    <td>position</td>
                    <td>trade.basePrice</td>
                    <td>trade.sizeToTrade</td>
                    <td>trade.active</td> */}

                        <td>
                          <PopupTable
                            trade_id={trade.trade_id}
                            status={trade.trade_position}
                            word="exitStrat"
                          />
                        </td>
                        <td>
                          <PopupTable trade_id={trade.trade_id} word="pause" />
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </CardBody>
            </Card>
          </Col>
        </Row>
        {/* <div>
          <Pagination
            className="pagination"
            items={this.state.currentHoldingsTable}
            onChangePage={this.onChangePage}
          />
        </div> */}
      </div>
    );
  }
}
export default TableData;
