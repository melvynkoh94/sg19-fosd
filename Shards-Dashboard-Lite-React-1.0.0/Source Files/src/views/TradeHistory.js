import React, { Component } from "react";
import { Container, Row, Col, Card, CardHeader, CardBody } from "shards-react";
import PaginationTable from "./Pagination/PaginationTable";

class TradeHistory extends Component {
  state = {
    pageCount: 1,
    recordsPerPage: 1,
    historyTable: []
  };

  componentDidMount() {
    fetch("http://app-fosd-project.dev2.conygre.com/allTradeHistory")
      .then(res => res.json())
      .then(data => {
        this.setState({ historyTable: data.response });
        console.log(this.state.historyTable);
      })
      .catch(console.log);
  }

  render() {
    return (
      <div>
        <PaginationTable historyTable={this.state.historyTable} />
      </div>
    );
  }
}
export default TradeHistory;
