/* eslint jsx-a11y/anchor-is-valid: 0 */

import React, { Component } from "react";
import PaginationTable_stocks from "./Pagination/PaginationTable_stocks";

import {
  Container,
  Row,
  Col,
  Card,
  CardBody,
  CardFooter,
  Badge,
  Button,
  CardHeader
} from "shards-react";

import PageTitle from "../components/common/PageTitle";
import ComponentsOverview from "./ComponentsOverview";

class BlogPosts extends Component {
  state = {
    stock_Table: []
  };
  constructor() {
    super();
    // this.handleSubmit = this.handleSubmit.bind(this);
    // state = {
    //   stock_Table: [
    //     {
    //       id: 1,
    //       stock_name: "Apple Inccc",
    //       stock_price: "1.1",
    //       stock_symbol: "aapl"
    //     },
    //     {
    //       id: 2,
    //       stock_name: "Citigroup Inc",
    //       stock_price: "5.1",
    //       stock_symbol: "c"
    //     }
    // ]
    // };
  }

  handleClick() {
    // e.preventDefault();
    console.log("The link was clicked.");
  }

  render() {
    const { data } = this.props.location;
    return (
      // <Container fluid className="main-content-container px-4"><

      <Container fluid className="main-content-container px-4">
        {/* Page Header */}
        <Row noGutters className="page-header py-4">
          <PageTitle
            sm="4"
            title="WELCOME TRADER"
            subtitle="FOSD PROJECT"
            className="text-sm-left"
          />
        </Row>

        <div>
          <PaginationTable_stocks />
        </div>
      </Container>
    );
  }
}
// <Container fluid className="main-content-container px-5">
//   {/* Page Header */}
//   <Row noGutters className="page-header py-4">
//     <PageTitle
//       sm="4"
//       title="Select your stock"
//       //subtitle="Components"
//       className="text-sm-left"
//     />
//     <br />
//     <br />
//     <br />
//   </Row>
//   {/* First Row of Posts */}
//   <Row>
//     {StockButtons1.map((stock, idx) => (
//       <Col className="mb-4" key={idx}>
//         {/* <Card small className="card-post card-post--1"> */}
//         <center>
//           <Button theme="primary" className="mb-2 mr-1 btn-lg btn-block">
//             {stock.stock_name}
//           </Button>
//         </center>
//       </Col>
//     ))}
//   </Row>
//   <Row className="mr-5 ml-5">
//     {StockButtons2.map((stock, idx) => (
//       <Col className="mb-4 mr-4 ml-4" key={idx}>
//         {/* <Card small className="card-post card-post--1"> */}
//         <center>
//           <Button theme="primary" className="mb-2 mr-1 btn-lg btn-block">
//             {stock.stock_name}
//           </Button>
//         </center>
//       </Col>
//     ))}
//   </Row>
// </Container>
function stringifyFormData(fd) {
  const data = {};
  for (let key of fd.keys()) {
    data[key] = fd.get(key);
  }
  return JSON.stringify(data, null, 2);
}
export default BlogPosts;
