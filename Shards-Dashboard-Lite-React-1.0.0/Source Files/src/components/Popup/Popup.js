import React from "react";
import "./Popup.css";

class Popup extends React.Component {
  getCorrectMessage() {
    // dummy stragetyid first 1-TMA, 2-BB, 3-SPB
    console.log("check type");
    console.log(this.props.popUpType);

    switch (this.props.popUpType) {
      case "exitStrat":
        return <p className="text">Are you sure you want to exit strategy?</p>;
      case "submitTMA":
        return <p className="text">Are you sure you want to submit TMA?</p>;
      case "pause":
        return <p className="text">Are you sure you want to pause?</p>;

      default:
        return null;
    }
  }
  render() {
    return (
      <div className="transparentBg">
        <div className="innerBg">
          {this.getCorrectMessage()}

          <button
            align="right"
            className="btn btn-danger btn-cancel mr-5"
            onClick={this.props.closePopup}
          >
            No
          </button>

          <button
            className="btn btn-success btn-continue"
            onClick={this.props.submitPopup}
          >
            Yes
          </button>
        </div>
      </div>
    );
  }
}

export default Popup;
