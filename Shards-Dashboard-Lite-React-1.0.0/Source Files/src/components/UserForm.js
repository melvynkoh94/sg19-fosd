import React, { Component } from "react";
import LetsGetStarted from "../components/components-overview/LetsGetStarted";
import StrategyDetails from "../components/components-overview/StrategyDetails";
import ComponentsOverview from "../views/ComponentsOverview";
// import FormPersonalDetails from "./FormPersonalDetails";
// import Confirm from "./Confirm";
// import Success from "./Success";

export class UserForm extends Component {
  state = {
    step: 1,
    size: "",
    strategy: ""
  };

  // Proceed to next step
  nextStep = () => {
    const { step } = this.state;
    this.setState({
      step: step + 1
    });
  };

  // Go back to prev step
  prevStep = () => {
    const { step } = this.state;
    this.setState({
      step: step - 1
    });
  };

  // Handle fields change
  handleChange = input => e => {
    this.setState({ [input]: e.target.value });
  };

  render() {
    const { step } = this.state;
    const { size, strategy } = this.state;
    const values = { size, strategy };

    switch (step) {
      case 1:
        return (
          <ComponentsOverview
            nextStep={this.nextStep}
            handleChange={this.handleChange}
            values={values}
          />
        );
      case 2:
        return (
          <StrategyDetails
            nextStep={this.nextStep}
            prevStep={this.prevStep}
            handleChange={this.handleChange}
            values={values}
          />
        );
      // case 3:
      //   return (
      //     <Confirm
      //       nextStep={this.nextStep}
      //       prevStep={this.prevStep}
      //       values={values}
      //     />
      //   );
      // case 4:
      //   return <Success />;
    }
  }
}

export default UserForm;
