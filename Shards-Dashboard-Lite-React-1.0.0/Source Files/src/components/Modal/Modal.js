import React from "react";

import "./Modal.css";

const expand = props => {
  return (
    <div>
      <div
        className="expand-wrapper"
        style={{
          transform: props.show ? "translateY(0vh)" : "translateY(-100vh)",
          opacity: props.show ? "1" : "0"
        }}
      >
        <div className="expand-header">
          <h3>Expand Header</h3>
          <span className="close-expand-btn" onClick={props.close}>
            ×
          </span>
        </div>
        <div className="expand-body">
          <p>{props.children}</p>
        </div>
        <div className="expand-footer">
          <button className="btn-cancel" onClick={props.close}>
            CLOSE
          </button>
          <button className="btn-continue">CONTINUE</button>
        </div>
      </div>
    </div>
  );
};

export default expand;
