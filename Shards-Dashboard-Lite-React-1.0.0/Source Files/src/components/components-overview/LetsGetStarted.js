//import React from "react";
import React, { Component } from "react";
import Select from "react-select";
import { history } from "react-router";

// import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
// import AppBar from "material-ui/AppBar";
// import TextField from "material-ui/TextField";
// import RaisedButton from "material-ui/RaisedButton";

import {
  ListGroup,
  ListGroupItem,
  Row,
  Col,
  Form,
  FormInput,
  FormGroup,
  FormCheckbox,
  FormSelect,
  FormFeedback,
  Button
} from "shards-react";

//import StrategyRadio from "../components-overview/StrategyRadio";

// const e = document.getElementById("chosenStrategy");
// const strUser = e.options[e.selectedIndex].text;
class ShakingError extends Component {
  constructor() {
    super();
    this.state = { key: 0 };
  }

  componentWillReceiveProps() {
    // update key to remount the component to rerun the animation
    this.setState({ key: ++this.state.key });
  }

  render() {
    return (
      <div key={this.state.key} className="bounce">
        {this.props.text}
      </div>
    );
  }
}

class LetsGetStarted extends Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {
      options: [
        { value: "TMA", label: "Two Moving Averages" },
        { value: "BB", label: "Bollinger Bands" },
        { value: "PB", label: "Price Breakout" }
      ]
    };
  }

  getStockName() {
    console.log("$$$$$d$$$$$$");
    console.log(this.props.location.state.stock_name);
    return this.props.location.state.stock_name;
  }

  handleSubmit(event) {
    event.preventDefault();
    const data = new FormData(event.target);

    this.setState({
      res: stringifyFormData(data),
      invalid: false,
      displayErrors: false
    });

    fetch("/api/form-submit-url", {
      method: "POST",
      body: data
    });
  }

  render() {
    const { res, invalid, displayErrors } = this.state;
    return (
      <ListGroup flush>
        <ListGroupItem className="p-3">
          <form onSubmit={this.handleSubmit}>
            <Row>
              <Col align="right">
                <FormGroup>
                  {/* pass stock name into {} */}
                  <label htmlFor="stock">Stock: </label>
                </FormGroup>
                <FormGroup>
                  {/* pass price into {} */}
                  <label htmlFor="stock">Current Price: {}</label>
                </FormGroup>
                <FormGroup>
                  <label htmlFor="quantity">Size to buy: </label>
                </FormGroup>
                <FormGroup className="pt-2">
                  <label htmlFor="strategy_choice">Choose a strategy: </label>
                </FormGroup>
                <FormGroup>
                  <label htmlFor="long_avg">Long Average: </label>
                </FormGroup>
                <FormGroup>
                  <label htmlFor="short_avg">Short Average: </label>
                </FormGroup>
              </Col>
              <Col>
                <FormGroup>
                  <label htmlFor="stock">{this.getStockName()}</label>
                </FormGroup>
                <FormGroup>
                  <label htmlFor="stock">eg. 1.1</label>
                </FormGroup>
                <FormGroup>
                  {/* <FormInput placeholder="Minimum 1" required /> */}
                  <FormInput id="size" name="size" placeholder="Minimum 1" />
                </FormGroup>
                <FormGroup>
                  <Select
                    id="strategy"
                    name="strategy"
                    options={this.state.options}
                  />
                </FormGroup>
                <FormGroup>
                  {/* <FormInput placeholder="Minimum 1" required /> */}
                  <FormInput
                    id="longAvg"
                    name="longAvg"
                    placeholder="Insert a value from 1 to 30"
                  />
                </FormGroup>
                <FormGroup>
                  {/* <FormInput placeholder="Minimum 1" required /> */}
                  <FormInput
                    id="shortAvg"
                    name="shortAvg"
                    placeholder="Insert a value from 1 to 30"
                  />
                </FormGroup>
              </Col>
            </Row>

            <Row className="mt-4">
              <Col>
                <div align="right">
                  {/* <Button outline size="lg" theme="dark" className="mb-2 mx-4">
                Clear
              </Button> */}
                  <Button
                    outline
                    size="lg"
                    className="mb-2 mx-4"
                    //primary={true}

                    //onClick={this.continue}
                  >
                    Submit
                  </Button>
                </div>
              </Col>
            </Row>
          </form>

          <div className="res-block">
            {invalid && <ShakingError text="Form is not valid" />}
            {!invalid && res && (
              <div>
                <h3>Transformed data to be sent:</h3>
                <pre>FormData {res}</pre>
              </div>
            )}
          </div>
        </ListGroupItem>
      </ListGroup>
    );
  }
}

// const LetsGetStarted = (values, handleChange) => (
//   <ListGroup flush>
//     <ListGroupItem className="p-3">
//       <Form>
//         <Row>
//           <Col align="right">
//             <FormGroup>
//               {/* pass stock name into {} */}
//               <label htmlFor="stock">Stock: </label>
//             </FormGroup>
//             <FormGroup>
//               {/* pass price into {} */}
//               <label htmlFor="stock">Current Price: {}</label>
//             </FormGroup>
//             <FormGroup>
//               <label htmlFor="quantity">Size: </label>
//             </FormGroup>
//             <FormGroup>
//               <label htmlFor="strategy_choice">Choose a strategy: </label>
//             </FormGroup>

//             {/* <ListGroupItem className="p-0 px-3 pt-3">
//               <Row> */}

//             {/* </Row>
//             </ListGroupItem> */}
//             {/* </Col> */}

//             {/* <Col md="6">
//                 <label htmlFor="fePassword">Password</label>
//                 <FormInput
//                   id="fePassword"
//                   type="password"
//                   placeholder="Password"
//                 />
//               </Col> */}
//             {/* </Row> */}
//           </Col>
//           <Col>
//             <FormGroup>
//               <label htmlFor="stock">eg. Singtel</label>
//             </FormGroup>
//             <FormGroup>
//               <label htmlFor="stock">eg. 1.1</label>
//             </FormGroup>
//             <FormGroup>
//               <FormInput placeholder="Minimum 1" required />
//             </FormGroup>
//             <FormGroup>
//               <FormSelect id="feInputStrategy">
//                 <option>Two-Moving Averages</option>
//               </FormSelect>
//             </FormGroup>
//           </Col>
//         </Row>
//         <Row className="mt-4">
//           <Col>
//             <div align="right">
//               <Button outline size="lg" theme="dark" className="mb-2 mx-4">
//                 Clear
//               </Button>
//               <Button
//                 // size="lg"
//                 // theme="primary"
//                 // className="mb-2 mx-4"
//                 // type="submit"size="lg"
//                 label="Continue"
//                 primary={true}
//                 //style={styles.button}
//                 //onClick={this.continue_}
//               >
//                 Next
//               </Button>
//             </div>
//           </Col>
//         </Row>
//       </Form>
//     </ListGroupItem>
//   </ListGroup>
// );
function stringifyFormData(fd) {
  const data = {};
  for (let key of fd.keys()) {
    data[key] = fd.get(key);
  }
  return JSON.stringify(data, null, 2);
}
export default LetsGetStarted;
