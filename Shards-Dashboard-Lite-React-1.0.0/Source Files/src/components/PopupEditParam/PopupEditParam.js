import React from "react";
import "./PopupEditParam.css";
import PopupEdit2MA from "./PopupEdit2MA";
import PopupEditBB from "./PopupEditBB";
import PopupEditPB from "./PopupEditPB";

class PopupEditParam extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showPopup: false
    };
  }
  getCorrectComopent() {
    // dummy stragetyid first 1-TMA, 2-BB, 3-PB
    // const strategyid = 1;

    switch (this.props.strategy_name) {
      case "TMA":
        return (
          <PopupEdit2MA
            tradeOrderId={this.props.tradeOrderId}
            strategyId={this.props.strategyId}
            strategy_name={this.props.strategy_name}
            sizeToTrade={this.props.sizeToTrade}
            longAvg={this.props.longAvg}
            shortAvg={this.props.shortAvg}
            interval={this.props.interval}
            symbol={this.props.symbol}
            edit={true}
          />
        );
      case "BB":
        return (
          <PopupEditBB
            tradeOrderId={this.props.tradeOrderId}
            strategyId={this.props.strategyId}
            strategy_name={this.props.strategy_name}
            sizeToTrade={this.props.sizeToTrade}
            standardDev={this.props.standardDev}
            windowSize={this.props.windowSize}
            symbol={this.props.symbol}
            edit={true}
          />
        );
      case "PB":
        return (
          <PopupEditPB
            tradeOrderId={this.props.tradeOrderId}
            strategyId={this.props.strategyId}
            strategy_name={this.props.strategy_name}
            sizeToTrade={this.props.sizeToTrade}
            period={this.props.period}
            symbol={this.props.symbol}
            edit={true}
          />
        );
      default:
        return null;
    }
  }
  render() {
    return (
      // <div className="transparentBg">
      //   <div className="innerBg">
      <div>
        {/* <button
          className="btn btn-danger btn-cancel mr-5"
          onClick={this.props.closePopup}
        >
          No
        </button> */}
        <div className="transparentBg_edit">
          <div className="innerBg_edit">
            <p
              align="right"
              onClick={this.props.closePopup}
              className="pr-4 pt-2"
            >
              X
            </p>

            <p className="text">
              {/* Are you sure you want to exit strategy #{this.props.strategyId}? */}
              Edit the parameters accordingly.
            </p>

            {/* need to be correct component to display. Call function here? */}
            {this.getCorrectComopent()}

            {/* <button
            className="btn btn-danger btn-cancel mr-5"
            onClick={this.props.closePopup}
          >
            No
          </button>
          <button
            className="btn btn-success btn-continue"
            onClick={this.props.submitPopup}
          >
            Yes
          </button> */}
          </div>
        </div>
      </div>
    );
  }
}

export default PopupEditParam;
