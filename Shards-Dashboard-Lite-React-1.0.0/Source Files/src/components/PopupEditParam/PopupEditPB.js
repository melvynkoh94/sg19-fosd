import React from "react";

import {
  Container,
  Row,
  Col,
  Card,
  CardHeader,
  ListGroup,
  ListGroupItem,
  Form,
  Alert,
  FormGroup,
  FormInput,
  Button
} from "shards-react";

//fetch again from the link@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

class PopupEditPB extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      paramTable: [],
      obj: null,
      period: null
      // showPopup: false
    };
    this.handleSubmit_PB = this.handleSubmit_PB.bind(this);
  }

  componentDidMount() {
    let url =
      "http://app-fosd-project.dev2.conygre.com/tradeOrders/" +
      this.props.strategy_name +
      "/test/" +
      this.props.strategyId;
    console.log("check URL");
    console.log(url);
    fetch(url)
      .then(res => res.json())
      .then(data => {
        this.setState({ paramTable: data });
        console.log(this.state.paramTable);
        this.state.obj = this.state.paramTable;
        this.setState({
          size_to_trade: this.state.obj["size_to_trade"],
          period: this.state.obj["period"]
        });
        // console.log(this.state.obj["period"]);
      })
      .catch(console.log);
  }

  handleSubmit_PB(event) {
    event.preventDefault();
    if (!event.target.checkValidity()) {
      this.setState({
        invalid: true,
        displayErrors: true
      });
      return;
    }
    const data = new FormData(event.target);

    this.setState({
      res: this.stringifyFormData(data),
      invalid: false,
      displayErrors: false
    });

    var dataObjToSend = JSON.parse(this.stringifyFormData(data));
    var dataForParam = JSON.parse(this.stringifyFormData(data));
    // var dataObjToSend = data;
    // var dataForParam = data;
    delete dataObjToSend["period"];
    delete dataObjToSend["symbol"];

    // dataObjToSend["stockId"] = this.props.location.state.stockId;
    dataObjToSend["timestamp"] = new Date();
    dataObjToSend["status"] = "Active";
    console.log(typeof dataObjToSend);
    console.log(dataObjToSend);
    console.log(typeof dataForParam);
    console.log(dataForParam["longAvg"]);
    if (this.props.edit) {
      console.log(dataForParam);
      let url =
        "http://192.168.8.198:8080/updatePB/" +
        this.props.strategyId +
        "?period=" +
        dataForParam["period"] * 60;
      fetch(url, {
        method: "PUT",
        headers: {
          // "Access-Control-Allow-Origin": "*",
          Accept: "application/json, application/xml",
          "Content-Type": "application/json"
        }
      })
        .then(response => {
          console.log(response);
          if (response.ok) {
            return response.json();
          } else {
            console.log("error");
            throw new Error("errorrrrr");
          }
        })
        .catch(error => this.setState({ error }));
    } else {
      let url =
        "http://192.168.8.198:8080/BPconfirm?period=" +
        dataForParam["period"] +
        "&symbol=" +
        dataForParam["symbol"];

      console.log("data to send");
      console.log(JSON.stringify(dataObjToSend));
      const body = dataObjToSend;
      fetch(url, {
        mode: "cors",
        method: "PUT",
        headers: {
          // "Access-Control-Allow-Origin": "*",
          Accept: "application/json, application/xml",
          "Content-Type": "application/json"
        },
        body: JSON.stringify(body)
      })
        .then(response => {
          console.log(response);
          if (response.ok) {
            return response.json();
          } else {
            console.log("error");
            throw new Error("errorrrrr");
          }
        })
        .catch(error => this.setState({ error }));
    }
  }

  stringifyFormData(fd) {
    const data = {};
    // const myform = {};
    for (let key of fd.keys()) {
      data[key] = fd.get(key);
      // myform[key] = fd.get(key);
    }
    // console.log(myform);
    return JSON.stringify(data, null, 2);
  }
  render() {
    return (
      <form onSubmit={this.handleSubmit_PB}>
        <Row className="pb-4">
          {/* <Col align="right">
            <FormGroup>
              
              <label>hi</label>
            </FormGroup>
          </Col> */}
          <Col align="right">
            <FormGroup>
              {/* pass stock name into {} */}
              <label style={{ color: "black" }}>Trade Order ID:</label>
            </FormGroup>
            <FormGroup>
              {/* pass price into {} */}
              <label style={{ color: "black" }} htmlFor="stock">
                Strategy ID:
              </label>
            </FormGroup>
            {/* <FormGroup>
              <label style={{ color: "black" }} htmlFor="quantity">
                Size to trade:
              </label>
            </FormGroup> */}
            <FormGroup>
              <label style={{ color: "black" }} htmlFor="long_avg">
                Period:
              </label>
            </FormGroup>
          </Col>
          <Col align="left">
            <FormGroup>
              <label style={{ color: "black" }} htmlFor="stock_name">
                {this.props.tradeOrderId}
              </label>
            </FormGroup>
            <FormGroup>
              <label style={{ color: "black" }} htmlFor="stock_price">
                {this.props.strategyId}
              </label>
            </FormGroup>

            {/* <FormGroup>
              <FormInput
                id="sizeOrdered"
                name="sizeOrdered"
                placeholder="Minimum 1"
                type="number"
                required
                min={1}
              />
            </FormGroup> */}

            <FormGroup>
              <FormInput
                id="period"
                name="period"
                placeholder="Value from 1 to 30"
                type="number"
                required
                min={1}
                max={30}
              />
            </FormGroup>
            <FormGroup>
              <input type="hidden" id="name" name="name" value="PB" />
            </FormGroup>
            <FormGroup>
              <input
                type="hidden"
                id="symbol"
                name="symbol"
                value={this.props.symbol}
              />
            </FormGroup>
          </Col>
          <Col align="left">
            <FormGroup>
              {/* pass stock name into {} */}
              <label />
            </FormGroup>
            <FormGroup>
              {/* pass stock name into {} */}
              <label style={{ color: "black" }}>Your Orignal Parameters:</label>
            </FormGroup>
            {/* <FormGroup className="pt-3">
              {/* pass stock name into {} }
              <label style={{ color: "green" }}>
                {this.state.size_to_trade}
              </label>
            </FormGroup> */}
            <FormGroup>
              {/* pass stock name into {} */}
              <label style={{ color: "green" }}>{this.state.period / 60}</label>
            </FormGroup>
          </Col>
        </Row>

        <Row className="mt-4">
          <Col>
            <div align="right">
              <Button outline size="lg" className="mb-2 mx-4" id="BB" name="BB">
                Submit PB
              </Button>
            </div>
          </Col>
        </Row>
      </form>
    );
  }
}

export default PopupEditPB;
