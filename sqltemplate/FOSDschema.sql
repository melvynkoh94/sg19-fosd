DROP SCHEMA IF EXISTS FOSD;
CREATE SCHEMA FOSD;

set global time_zone = '+3:00';
set foreign_key_checks=0;
CREATE TABLE FOSD.stock (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    symbol VARCHAR(15) NOT NULL
);

CREATE TABLE FOSD.stock_price (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    stock_id INT NOT NULL,
    timestamp TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    price DOUBLE NOT NULL,
    foreign KEY (stock_id) REFERENCES FOSD.stock (id)
);

CREATE TABLE FOSD.strategy_order (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    stock_id INT NOT NULL,
    size_ordered INT NOT NULL,
    timestamp TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    status VARCHAR(50) NOT NULL,
    FOREIGN KEY (stock_id) REFERENCES FOSD.stock (id)
);

CREATE TABLE FOSD.trade_order (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    strategy_order_id INT NOT NULL,
    position VARCHAR(50) NOT NULL,
    trade_size INT NOT NULL,
    is_entering BOOL NOT NULL,
    status VARCHAR(50) NOT NULL,
    base_price DOUBLE NOT NULL DEFAULT 0,
    is_partial_filled BOOL NOT NULL,
    FOREIGN KEY (strategy_order_id) REFERENCES FOSD.strategy_order (id)
);

CREATE TABLE FOSD.two_moving_average_order (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    strategy_order_Id INT NOT NULL,
    long_avg_window INT NOT NULL,
    short_avg_window INT NOT NULL,
    period INT NOT NULL,
    FOREIGN KEY (strategy_order_Id) REFERENCES FOSD.strategy_order (id)
);

CREATE TABLE FOSD.boillinger_order (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    strategy_order_Id INT NOT NULL,
    std_deviation DOUBLE NOT NULL,
    avg_window INT NOT NULL,
    FOREIGN KEY (strategy_order_Id) REFERENCES FOSD.strategy_order (id)
);

CREATE TABLE FOSD.price_breakout_order (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    strategy_order_Id INT NOT NULL,
    period INT NOT NULL,
    FOREIGN KEY (strategy_order_Id) REFERENCES FOSD.strategy_order (id)
);

CREATE TABLE FOSD.trade_history (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    strategy_order_Id INT NOT NULL,
    is_long_position BOOL NOT NULL,
    size_traded INT NOT NULL,
    price DOUBLE NOT NULL,
    result VARCHAR(50) NOT NULL,
    timestamp TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (strategy_order_Id) REFERENCES FOSD.strategy_order (id)
);
USE FOSD;
set foreign_key_checks=1;
