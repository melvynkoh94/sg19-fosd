# FOSD Trading Platform Documentation

## Getting Started



## Contents

[TOC]

## Overview of Application

The overall architectural structure of the FOSD Trading Platform system is as below: 

![System Architecture](system-architecture.png "System Architecture")



## User Interface (UI)

The user interface is built on React, a JavaScript UI library. 

Some functionalities that are included are: 

1. **Trade Strategy Selection** - Allows user to select stock and trading strategy
2. **Performance Analysis** - Displays overall return of investment  
3. **Trade History Table and Current Holdings Table** - Records past trades and ongoing strategy orders
4. **Pause or Exit a Strategy** - Allows users to re-activate/de-activate or exit from a strategy position
5. **Live Feed of Stock Prices**



## Core Application

The main application is developed in Spring Boot. The core business logic includes: 

#### Trading Strategies

1. **Two-Moving Averages**
   - Triggers a trade position when the short average **“crosses”** the long average.
2. **Bollinger Bands**
   - Takes advantage of volatility in a stock.
   - Takes position when the stock price **hits** some multiple of the **standard deviation**. 
3. **Price Breakout**
   - Takes advantage of upcoming volatility. 
   - Enters a trade when stock price is **repeatedly “breaking out”** of its usual price range.

The **exit criteria** for all strategies is when there exist a **profit/loss of 1%**.



#### REST Controllers

Implementation of REST API to communicate with the REST client, i.e. the UI web-server. POST, PUT and GET methods are used to respond and request information. 



#### Testing

Unit testing is done using the JUnit test framework. 



#### Mock Order Broker

Trade order requests are sent to the message broker via JMS Messaging using ActiveMQ. The order broker will then send the response of the result of the trade back to the system. 



## Database

The database are handled in MySQL. The database records strategy orders, trade history and historical stock prices. 

The Entity Relationship diagram of the database is as shown below: 

![FOSD ER Diagram](fosd-ER.png "FOSD ER Diagram")



## Deployment

Docker is used as a containerization tool and it is being run on Openshift. This deploys and runs the application in a virtualized environment. 

There are 4 Docker Containers in total:

1. User Interface - React 
2. Main Application - Spring
3. Database - MySQL
4. Mock Order Broker - ActiveMQ





